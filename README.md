# hostel_admission

For ML code to run first install composer using sudo apt install composer



In order to use Tesseract OCR you may need to follow following steps:

1) Install Tesseract OCR into your system For installation

    please checkout:https://github.com/tesseract-ocr/tesseract/wiki.

    For Ubuntu Linux System you can run :

    sudo apt-get install tesseract-ocr

2) Make composer.json file with following content:

    {"require":{"thiagoalessio/tesseract_ocr": "1.0.0-RC"}}

3) Execute command from terminal

    composer install

4) Finally, Do PHP Code:

    require_once "vendor/autoload.php";

    echo (new TesseractOCR('test.png'))->run();
