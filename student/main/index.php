<?php
include('../../auth.php');
require_once('../../connection.php');
$mis = $_SESSION['mis'];
$sql = "SELECT * FROM tabs";
$result = $conn->query($sql);
$data = $result->fetch_assoc();
if(isset($data['openform']) && $data['openform'] == '0') {
  $openform = 'style="pointer-events: none;opacity: 0.4;"';
}
if(isset($data['openlist']) && $data['openlist'] == '0') {
  $openlist = 'style="pointer-events: none;opacity: 0.4;"';
}
if(isset($data['openroomallocation']) && $data['openroomallocation'] == '0') {
  $openroomallocation = 'style="pointer-events: none;opacity: 0.4;"';
}
if(isset($data['openroomlist']) && $data['openroomlist'] == '0') {
  $openroomlist = 'style="pointer-events: none;opacity: 0.4;"';
}
$sql = "select comments,accept,fname,lname,block from student where mis = '$mis'";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
  while($row = $result->fetch_assoc()) {
    if($row['block'] == '0') {
      // echo "<script>alert('Yooopioioppuoiuoiuipo');</script>";
      $allcomments = $row['comments'] ;
      $val = $row['accept'];
      $name = $row['fname'].' '.$row['lname'];
      $_SESSION['name'] = $name;
      $_SESSION['val'] = $val;
    }
    else {
      echo "<script>window.confirm('You are not eligible for Hostel Admission process');window.location.replace(\"logout.php\");</script>";
      // header('Location: logout.php');
    }
  }
  $commentlist = explode(',', $allcomments);
  $sqlAccept = "SELECT * from roommates where main = '$mis' or roommate1 = '$mis' or roommate2 = '$mis' or roommate3 = '$mis'" ;
  $result = $conn->query($sqlAccept);
  $row = $result->fetch_assoc();
  if($row && $row['main'] != $mis){
    $acceptReject = 'style="display : block"';
    $members = array();
    array_push($members, $row['main']);
    array_push($members, $row['roommate1']);
    array_push($members, $row['roommate2']);
    array_push($members, $row['roommate3']);
    if (($key = array_search($mis, $members)) !== false) {
      array_splice($members, $key, 1);
    }
  }
  else{
    $acceptReject = 'style="display : none"';
  }
}
else{
  echo 'Invalid mis'.' '.$mis;
}





?>
<!DOCTYPE html>
<html lang="en">
  <head>
     <link href="../vendors/pnotify/dist/pnotify.css" rel="stylesheet">
    <link href="../vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
    <link href="../vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>COEP | HOSTEL ADMISSION</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
	
    <!-- bootstrap-progressbar -->
    <link href="../vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="../vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="../vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
    <script>
    </script>
  </head>

  <body class="nav-md" onload="closePnotify();">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.php" class="site_title"><i class="fa fa-paw"></i> <span>COEP HOSTEL</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="images/user.png" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2><?php if(isset($name)) echo $name; ?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <ul class="nav side-menu">
                  <li><a href="index.php"><i class="fa fa-home"></i> Home</a>
                  </li>
                  <li <?php if(isset($openform)) echo $openform;?>><a><i class="fa fa-edit"></i> Forms <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                    <li><a href="admissionform.php"><?php if(!isset($val) || $val < 1) echo "Admission form"; else echo "Form Status";?></a></li>
                    </ul>
                  </li>
                  <li <?php if(isset($openlist)) echo $openlist;?>><a><i class="fa fa-building"></i>Allotment List<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                    <li><a href="seatdisplayaccepted.php"><i class="fa fa-check"></i>Alloted List</a></li>
                    <li><a href=seatdisplaywaiting.php><i class="fa fa-clock-o"></i>Waiting List</a></li>
                    </ul>
                  </li>
                  <li <?php if(isset($openroomallocation)) echo $openroomallocation;?>><a href="invoice.php"><i class="fa fa-credit-card-alt"></i>Fees Payment</span></a>
                  </li>
                  <li <?php if(isset($openroomlist)) echo $openroomlist;?>><a href="roomlist.php"><i class="fa fa-file-pdf-o"></i>Room Allotment List</span></a>
                  </li>
                </ul>
              </div>

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
           <!--  <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div> -->
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="images/user.png" alt=""><?php echo $name; ?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="logout.php"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <!-- top tiles -->
            <!-- Done with notice -->
          <div class="row">
            <div class="col-md-9 col-sm-9 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><i class="fa fa-bars"></i> Notices Board<small></small></h2>
                   
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">


                    <div class="" role="tabpanel" data-example-id="togglable-tabs">
                      <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">All Notices</a>
                        </li>
                        <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Important Notices</a>
                        </li>
                        <li role="presentation" class=""><a href="#tab_content3" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Form Links</a>
                        </li>
                      </ul>
                      <div id="myTabContent" class="tab-content">
                        <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                          <ul class="list-group">
                              <?php 
                                      $sql = "SELECT content,date,author FROM notices";
                                      $result = $conn->query($sql);
                                      if ($result->num_rows > 0) {
                                        // output data of each row
                                        $num = 0;
                                        $color = array("'list-group-item list-group-item-success'", "'list-group-item list-group-item-danger'"); 
                                        while($row = $result->fetch_assoc()) {
                                      ?>
                                      
                                      <div class="media">
                                        <div class="media-body">
                                          <span class="media-meta pull-right"><?php echo $row['date']; ?></span>
                                           <h4 class="title"><?php echo $row['author']; ?>
                                          </h4>
                                          <p class="summary"><?php echo $row['content'];?></p>
                                        </div>
                                      </div>
                                    <!--<h2><li class ="list-group-item"> <strong><?php echo $row['content'];?></strong> </li></h2> -->
                                    <?php
                                        if($num == 0){
                                          $num = 1;
                                          }
                                          else{
                                            $num = 0;
                                          }
                                        }
                                        }else {

                                          $count = 0;
                                      }
                                    ?>
                          </ul>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
                            <ul class="list-group"> 
                              <?php 
                                      $sql = "SELECT content,date,author FROM notices limit 2";
                                      $result = $conn->query($sql);
                                      if ($result->num_rows > 0) {
                                        // output data of each row
                                        $num = 0;
                                        $color = array("'list-group-item list-group-item-success'", "'list-group-item list-group-item-danger'"); 
                                        while($row = $result->fetch_assoc()) {
                                      ?>
                                      
                                      <div class="media">
                                        <div class="media-body">
                                          <span class="media-meta pull-right"><?php echo $row['date']; ?></span>
                                           <h4 class="title"><?php echo $row['author']; ?>
                                          </h4>
                                          <p class="summary"><?php echo $row['content'];?></p>
                                        </div>
                                      </div>
                                      <!--<h2><li class ="list-group-item"> <strong><?php echo $row['content'];?></strong> </li></h2> -->
                                    <?php
                                        if($num == 0){
                                          $num = 1;
                                          }
                                          else{
                                            $num = 0;
                                          }
                                        }
                                        }else {

                                          $count = 0;
                                      }
                                    ?>
                          </ul>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab">
                          <ul class="list-group">
                              <li role="presentation" class="list-group"><a href = "../../upload/admindocs/F.Y.B.TECH. 4RD ALLOTMENT LIST 17.08.2016.pdf">F.Y.B.TECH. 4RD ALLOTMENT LIST</a></li>
                              <li role="presentation" class="list-group"><a href = "../../upload/admindocs/F.Y.B.TECH (BOYS) PROVISIONAL  HOSTEL ADMISSION LIST 2017-18 Round-2 DATE 09.08.17.xls">F.Y.B.TECH (BOYS) PROVISIONAL  HOSTEL ADMISSION LIST 2017-18 Round-2 DATE 09.08.17</a></li>
                              <li role="presentation" class="list-group"><a href = "../../upload/admindocs/(GIRLS) F.Y.B.TECH HOSTEL ALLOTMENT 2017-18.pdf">(GIRLS) F.Y.B.TECH HOSTEL ALLOTMENT 2017-18</a> </li>
                              <li role="presentation" class="list-group"><a href = "../../upload/admindocs/Hostel Admission Notification 2016-17.pdf">Hostel Admission Notification 2016-17</a> 
                              <a href = "../../upload/admindocs/seatdistribution.pdf">Allocation </a> </li>
                          </ul>
                        </div>
                      </div>
                    </div>

                  </div>
                </div>
              </div>
              <!-- Done with notice -->

              <!-- -->
                <div class="col-md-3 col-sm-3 col-xs-12">
                 <div class="x_panel">
                <!--  <div class="x_title">
                 -->    <h2><i class="fa fa-bars"></i> Message From Admin<small></small></h2>
                      <ul class="list-group">
                        <?php
                          foreach ($commentlist as $key => $value) {
                            
                        ?>
                        <div class="media">
                          <div class="media-body">
                            <span class="media-meta pull-right">12/2/2018</span>
                             <h4 class="title">Mr.Satare
                            </h4>
                            <p class="summary"><?php echo $value;?></p>
                          </div>
                        </div>
                        <?php
                          }
                        ?>
                      </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                </div>
              </div>
            </div>

            <div class = "row" id="butme" <?php echo $acceptReject;?>>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Accept/Reject  <small>Room-mate selection</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div class="row">
                      <button type="button" class="btn btn-info col-md-4 col-lg-4" data-toggle="tooltip" data-placement="left"><?php if(isset($members[0])) echo $members[0];?></button>

                      <button type="button" class="btn btn-info col-md-4 col-lg-4" data-toggle="tooltip" data-placement="top" ><?php if(isset($members[1])) echo $members[1];?></button>

                      <button type="button" class="btn btn-info col-md-3 col-lg-3" data-toggle="tooltip" data-placement="right" ><?php if(isset($members[2])) echo $members[2];?></button>
                    </div>
                      <div class="row"></div>
                      <div class="row"></div>
                      <div class="row">
                        <div class = "col-md-2"></div>    
                        <button type="button" class="btn btn-primary col-md-4 col-lg-4" data-placement="left" onclick="dismiss(0);" id = "acceptBut">Accept</button>

                        <div class = "col-md-2"></div>
                        <form action="#" method='post' name='acrj'>
                        <!-- <input type="hidden" name="rejection" id="rejection"> -->
                        <button type="button" class="btn btn-danger col-md-4 col-lg-4"  data-placement="right" onclick="dismiss(1);" id = "acceptBut1">Reject</button>
                        </form>
                          <div class="row">
                          <!-- end pop-over -->

                        </div>
                      </div>
                    </div>
                </div>
            </div>
          </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            COEP HOSTEL ADMISSION PORTAL
          </div>
          <div class="clearfix"></div>
        </footer>
          </div>
        
        <!-- /footer content -->
      </div>
    </div>
    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="../vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="../vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="../vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="../vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="../vendors/Flot/jquery.flot.js"></script>
    <script src="../vendors/Flot/jquery.flot.pie.js"></script>
    <script src="../vendors/Flot/jquery.flot.time.js"></script>
    <script src="../vendors/Flot/jquery.flot.stack.js"></script>
    <script src="../vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="../vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="../vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="../vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="../vendors/DateJS/build/date.js"></script>
    <!-- JQVMap -->
    <script src="../vendors/jqvmap/dist/jquery.vmap.js"></script>
    <script src="../vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="../vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="../vendors/moment/min/moment.min.js"></script>
    <script src="../vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>
    <script type="text/javascript">
      var mis = <?php echo $mis ?>;
      
     
      



      function dismiss(val){
        // document.getElementById('rejection').value ='set';
        document.getElementById('butme').style = "display:none";
        if(val == 1) {
          var jQueryfunction = $.post("rejection.php",{mis:mis},function(response,status){
              alert(response); 
          })
        }
      }
      function closePnotify() {
        document.getElementsByClassName('ui-pnotify-text')[0].innerHTML="You have an urgent notification.<br> Scroll down to check.";
        setTimeout("document.getElementsByClassName('ui-pnotify-container')[0].style.display = 'none';",5000);
      }
    </script>
     <script src="../vendors/pnotify/dist/pnotify.js"></script>
    <script src="../vendors/pnotify/dist/pnotify.buttons.js"></script>
    <script src="../vendors/pnotify/dist/pnotify.nonblock.js"></script>
  </body>
</html>
<?php $conn->close(); ?>
