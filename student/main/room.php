<?php
error_reporting(0); 
include('../../auth.php');
require_once('../../connection.php');
$mis = $_SESSION['mis'];
$sql = "SELECT * FROM tabs";
$result = $conn->query($sql);
$data = $result->fetch_assoc();
if(isset($data['openform']) && $data['openform'] == '0') {
  $openform = 'style="pointer-events: none;opacity: 0.4;"';
}
if(isset($data['openlist']) && $data['openlist'] == '0') {
  $openlist = 'style="pointer-events: none;opacity: 0.4;"';
}
if(isset($data['openroomallocation']) && $data['openroomallocation'] == '0') {
  $openroomallocation = 'style="pointer-events: none;opacity: 0.4;"';
}
if(isset($data['openroomlist']) && $data['openroomlist'] == '0') {
  $openroomlist = 'style="pointer-events: none;opacity: 0.4;"';
}
// $sql11 = "update student set accept = 5 where mis = '$mis'";
// $result = $conn->query($sql11);
$sql = "select year,gender,branch,cgpa from student where mis = '$mis'";
$result = $conn->query($sql);
$year = 0;
$gender = 0;
$branch = "";
if ($result->num_rows > 0) {
  while($row = $result->fetch_assoc()) {
     $year = $row['year'];
     $gender = $row['gender']; 
     $branch = $row['branch'];
     $cgpa = $row['cgpa'];
  }
  $str = 'python3 svm.py '.$cgpa.' '.$branch.' 2>&1';
  //echo $str;
  exec($str, $output, $ret_code);
  //echo $ret_code;
  if($ret_code == '1'){
    echo "<script>alert('Predicted Rooms for you are E105,E201,E305')</script>";
  }
  elseif($ret_code == '0'){
    echo "<script>alert('Predicted Rooms for you are F101,F106,F201')</script>";
  }
  else{
    echo "<script>alert('Predicted Rooms for you are G103,G206,G208')</script>";
  }
  
  // echo $mis.' '.$year.' '.$gender;
}
else{
  echo 'Invalid mis'.' '.$mis.' '.$year;
}
  $firstgirls = "style='display:none'";
  $secondgirls = "style='display:none'";
  $thirdgirls = "style='display:none'";
  $fourthgirls = "style='display:none'";
  $firstboys = "style='display:none'";
  $secondboys = "style='display:none'";
  $thirdboys = "style='display:none'";
  $fourthboys = "style='display:none'";

  if($year == 1 && $gender == 0){ //for girl from 1st year
    $firstgirls = "style='display:block'";
  }
  elseif($year == 1 && $gender == 1){
    $firstboys = "style='display:block'"; 
  }
  elseif($year == 2 && $gender == 0){
    $secondgirls = "style='display:block'"; 
  }
  elseif($year == 2 && $gender == 1){
    $secondboys = "style='display:block'"; 
  }
  elseif($year == 3 && $gender == 0){
    $thirdgirls = "style='display:block'"; 
  }
  elseif($year == 3 && $gender == 1){
    $thirdboys = "style='display:block'"; 
  }
  elseif($year == 4 && $gender == 0){
    $fourthgirls = "style='display:block'"; 
  }
  elseif($year == 4 && $gender == 1){
    $fourthboys = "style='display:block'"; 
  }
  // echo $secondgirls;



  if(isset($_POST['pref1'])) {
      $prefdata = $_POST['pref1'];
      echo $prefdata;
      $sql = "INSERT INTO `priority`(`mis`, `priority`) VALUES ('$mis','$prefdata')";
      try {
        $conn->query($sql);
        echo "<script>alert('Successfully submitted..!');</script>";
        header('Location: index.php');
      }
    catch (Exception $e){
      echo "<script>alert('Couldn't save the the choices');</script>";
    }

  }
?>
<html>
  <head>
    <style>
    
    </style>
  <title>COEP | HOSTEL ADMISSION
  </title>
  
  <!-- // ============================================ -->

   <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

   

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    
    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">




<link href="../vendors/pnotify/dist/pnotify.css" rel="stylesheet">
    <link href="../vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
    <link href="../vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">
<style type="text/css">
 #overlay {
    position: fixed;
    display: none;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    right: 0;
    bm: 0;
    background-color: rgba(0,0,0,0.8);
    z-index: 2;
    cursor: pointer;
}

#text{
    position: absolute;
    top: 50%;
    left: 50%;
    font-size: 50px;
    color: white;

    transform: translate(-50%,-50%);
    -ms-transform: translate(-50%,-50%);
}
.btn {
    border-width: 0 4px 5px 0;
  border-radius: 5px;
  border-color: transparent #ddd #999 transparent;
  background-clip: padding-box;  
}
  .btn:active {
     background: #bada55;      
      -webkit-animation-name: rubberBand;
          animation-name: rubberBand;
      animation-duration: 300ms;
      animation-fill-mode: both;
  }

  .btn:disabled {
     background: green;      
      -webkit-animation-name: rubberBand;
          animation-name: rubberBand;
      animation-duration: 300ms;
      animation-fill-mode: both;
  }

  
  @-webkit-keyframes rubberBand {
  0% {
    -webkit-transform: scale3d(1, 1, 1);
            transform: scale3d(1, 1, 1);
  }

  30% {
    -webkit-transform: scale3d(1.25, 0.75, 1);
            transform: scale3d(1.25, 0.75, 1);
  }

  40% {
    -webkit-transform: scale3d(0.75, 1.25, 1);
            transform: scale3d(0.75, 1.25, 1);
  }

  50% {
    -webkit-transform: scale3d(1.15, 0.85, 1);
            transform: scale3d(1.15, 0.85, 1);
  }

  65% {
    -webkit-transform: scale3d(.95, 1.05, 1);
            transform: scale3d(.95, 1.05, 1);
  }

  75% {
    -webkit-transform: scale3d(1.05, .95, 1);
            transform: scale3d(1.05, .95, 1);
  }

  100% {
    -webkit-transform: scale3d(1, 1, 1);
            transform: scale3d(1, 1, 1);
  }
}

@keyframes rubberBand {
  0% {
    -webkit-transform: scale3d(1, 1, 1);
            transform: scale3d(1, 1, 1);
  }

  30% {
    -webkit-transform: scale3d(1.25, 0.75, 1);
            transform: scale3d(1.25, 0.75, 1);
  }

  40% {
    -webkit-transform: scale3d(0.75, 1.25, 1);
            transform: scale3d(0.75, 1.25, 1);
  }

  50% {
    -webkit-transform: scale3d(1.15, 0.85, 1);
            transform: scale3d(1.15, 0.85, 1);
  }

  65% {
    -webkit-transform: scale3d(.95, 1.05, 1);
            transform: scale3d(.95, 1.05, 1);
  }

  75% {
    -webkit-transform: scale3d(1.05, .95, 1);
            transform: scale3d(1.05, .95, 1);
  }

  100% {
    -webkit-transform: scale3d(1, 1, 1);
            transform: scale3d(1, 1, 1);
  }
}

.rubberBand {
  -webkit-animation-name: rubberBand;
          animation-name: rubberBand;
}


</style>
  <script type="text/javascript">


      var roomchoice = new Array();
      var objlist = new Array();
      var i = 0;
      function select1(obj,value) {
        obj.disabled = true;
         obj.className = "btn btn-danger";
        roomchoice.push(value);
        // objlist[i] = new Object();
        // objlist.push(obj);
        // alert(objlist[i]);
        i++;
      }
      function reset1() {
        for(var j = 0;j < i; j++) {
          document.getElementById(roomchoice[j]).disabled = false;
           document.getElementById(roomchoice[j]).className ="btn btn-default";
          // alert(roomchoice[j]);
        }
        i = 0;
        roomchoice = new Array();

      }
      function submit_pref() {
        // alert("uoo");
        preference = "";
        for(val in roomchoice) {
          preference += roomchoice[val]+',';
        }
        console.log(preference);
        if(preference.length > 0) {
          preference = preference.slice(0, -1);
        }
        console.log(roomchoice);
        console.log(preference);
        alert(preference);
        document.getElementById('pref1').value = preference;
       //alert(preference);
        document.getElementById('roomallot').submit();
      }
      function openModal1() {
        setTimeout('document.getElementById("modal_button").click();',1000)
        
      }
  </script>
  </head>
 <body class="nav-md" onload="openModal1()">
    <div class="container body">
    
      <div class="main_container">
      

  
  <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.php" class="site_title"><i class="fa fa-paw"></i> <span>COEP HOSTEL</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="images/user.png" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2><?php if(isset($_SESSION['name'])) echo $_SESSION['name']; ?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3></h3>
                <ul class="nav side-menu">
                  <li><a href="index.php"><i class="fa fa-home"></i> Home</a>
                  </li>
                  <li <?php if(isset($openform)) echo $openform;?>><a><i class="fa fa-edit"></i> Forms <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                    <li><a href="admissionform.php"><?php if(isset($val) && $val < 1) echo "Admission form"; else echo "Form Status";?></a></li>
                    </ul>
                  </li>
                  <li <?php if(isset($openlist)) echo $openlist;?>><a><i class="fa fa-building"></i>Allotment List<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                    <li><a href="seatdisplayaccepted.php"><i class="fa fa-check"></i>Alloted List</a></li>
                    <li><a href=seatdisplaywaiting.php><i class="fa fa-clock-o"></i>Waiting List</a></li>
                    </ul>
                  </li>
                  <li <?php if(isset($openroomallocation)) echo $openroomallocation;?>><a href="invoice.php"><i class="fa fa-credit-card-alt"></i>Fees Payment</span></a>
                  </li>
                  <li <?php if(isset($openroomlist)) echo $openroomlist;?>><a href="roomlist.php"><i class="fa fa-file-pdf-o"></i>Room Allotment List</span></a>
                  </li>
                </ul>
              </div>

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
           <!--  <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div> -->
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="images/user.png" alt=""><?php if(isset($_SESSION['name'])) echo $_SESSION['name']; ?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    
                    <li><a href="logout.php"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>

                <li role="presentation" class="dropdown">
                  <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                    
                  </a>
                  
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->
 <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Room Preferences</h3>
              </div>
            </div>


                <div class="col-md-12 col-sm-12 col-xs-12 form-group pull-right top_search">
              <!-- ithe block takaicha -->
             </div> 
                <div class="col-md-12 col-sm-12 col-xs-12 form-group pull-right top_search">

    <form id="roomallot" name="roomallot" method="post" action="#">
      <div id="EBLOCK" <?php echo $thirdboys; ?> >

      
                <div class="x_panel">
                  <div class="x_title">
                   <a onmouseover="document.getElementById('imag12').style='display:block;width:auto%;height:auto;'" onmouseout="document.getElementById('imag12').style='display:none;'">
                     <h2>E-Block</h2>
                    </a> &nbsp;
                      <img src="images/hostel.jpeg" id="imag12" style="display: none;" alt="ROOM IMAGE">
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <table class="table">
                      <thead>
                        <tr>
                          <th>Floor</th>
                          <th>ROOMS</th>
                          <th>S</th>
                          <th>Washroom</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <th scope="row">1</th>
                          <td><input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="E101" value="E101">
      <input type="button" class="btn btn-default" onclick="select1(this,this.value)" id="E102" value="E102">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="E103" value="E103">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="E104" value="E104">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="E105" value="E105">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="E106" value="E106">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="E107" value="E107">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="E108" value="E108">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="E109" value="E109">
      </td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <th scope="row">2</th>
                          <td>
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="E201" value="E201">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="E202" value="E202">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="E203" value="E203">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="E204" value="E204">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="E205" value="E205">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="E206" value="E206">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="E207" value="E207">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="E208" value="E208">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="E209" value="E209">
      </td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <th scope="row">3</th>
                          <td>  <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="E301" value="E301">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="E302" value="E302">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="E303" value="E303">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="E304" value="E304">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="E305" value="E305">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="E306" value="E306">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="E307" value="E307">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="E308" value="E308">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="E309" value="E309">
    </td>
                          <td></td>
                          <td></td>
                        </tr>
                      </tbody>
                    </table>

                  </div>
                </div>

      <!-- <br> -->
      <br>
      </div>

      <!-- Fourth boys-->
        <div id="CBLOCK" <?php echo $fourthboys; ?> >

      
                <div class="x_panel">
                  <div class="x_title">
                    <a onmouseover="document.getElementById('imag2').style='display:block;width:auto%;height:auto;'" onmouseout="document.getElementById('imag2').style='display:none;'"><h2>C-Block</h2>
                    </a> &nbsp;
                      <img src="images/img.jpg" id="imag2" style="display: none;" alt="ROOM IMAGE">
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <table class="table">
                      <thead>
                        <tr>
                          <th>Floor:</th>
                          <th>ROOMS</th>
                          <th>S</th>
                          <th>Washroom</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <th scope="row">1</th>
                          <td><input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="C101" value="C101">
       <input  type="button" class="btn btn-default" onclick="select1(this,this.value)" id="C102" value="C102">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="C103" value="C103">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="C104" value="C104">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="C105" value="C105">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="C106" value="C106">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="C107" value="C107">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="C108" value="C108">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="C109" value="C109">
      </td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <th scope="row">2</th>
                          <td>
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="C201" value="C201">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="C202" value="C202">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="C203" value="C203">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="C204" value="C204">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="C205" value="C205">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="C206" value="C206">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="C207" value="C207">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="C208" value="C208">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="C209" value="C209">
      </td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <th scope="row">3</th>
                          <td>  <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="C301" value="C301">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="C302" value="C302">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="C303" value="C303">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="C304" value="C304">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="C305" value="C305">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="C306" value="C306">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="C307" value="C307">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="C308" value="C308">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="C309" value="C309">
    </td>
                          <td></td>
                          <td></td>
                        </tr>
                      </tbody>
                    </table>

                  </div>
                </div>

      <!-- <br> -->
      <br>
      </div>
    <div id="DBLOCK" <?php echo $fourthboys; ?> >

      
                <div class="x_panel">
                  <div class="x_title">
                   <a onmouseover="document.getElementById('imag3').style='display:block;width:auto%;height:auto;'" onmouseout="document.getElementById('imag3').style='display:none;'"> <h2>D-Block</h2>
                    </a> &nbsp;
                      <img src="images/img.jpg" id="imag3" style="display: none;" alt="ROOM IMAGE">
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <table class="table">
                      <thead>
                        <tr>
                          <th>Floor:</th>
                          <th>ROOMS</th>
                          <th>S</th>
                          <th>Washroom</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <th scope="row">1</th>
                          <td><input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="D101" value="D101">
      <input type="button" class="btn btn-default" onclick="select1(this,this.value)" id="D102" value="D102">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="D103" value="D103">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="D104" value="D104">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="D105" value="D105">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="D106" value="D106">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="D107" value="D107">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="D108" value="D108">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="D109" value="D109">
      </td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <th scope="row">2</th>
                          <td>
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="D201" value="D201">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="D202" value="D202">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="D203" value="D203">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="D204" value="D204">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="D205" value="D205">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="D206" value="D206">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="D207" value="D207">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="D208" value="D208">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="D209" value="D209">
      </td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <th scope="row">3</th>
                          <td>  <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="D301" value="D301">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="D302" value="D302">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="D303" value="D303">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="D304" value="D304">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="D305" value="D305">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="D306" value="D306">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="D307" value="D307">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="D308" value="D308">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="D309" value="D309">
    </td>
                          <td></td>
                          <td></td>
                        </tr>
                      </tbody>
                    </table>

                  </div>
                </div>

      <!-- <br> -->
      <br>
      </div>

      <!-- fourth boys end-->

        <div id="GBLOCK" <?php echo $thirdboys; ?> >

      
                <div class="x_panel">
                  <div class="x_title">
                    <a onmouseover="document.getElementById('imag4').style='display:block;width:auto%;height:auto;'" onmouseout="document.getElementById('imag4').style='display:none;'"><h2>G-Block</h2>
                    </a> &nbsp;
                      <img src="images/img.jpg" id="imag4" style="display: none;" alt="ROOM IMAGE">
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <table class="table">
                      <thead>
                        <tr>
                          <th>Floor:</th>
                          <th>ROOMS</th>
                          <th>S</th>
                          <th>Washroom</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <th scope="row">1</th>
                          <td>
            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="G101" value="G101">
      <input type="button" class="btn btn-default" onclick="select1(this,this.value)" id="G102" value="G102">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="G103" value="G103">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="G104" value="G104">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="G105" value="G105">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="G106" value="G106">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="G107" value="G107">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="G108" value="G108">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="G109" value="G109">
      </td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <th scope="row">2</th>
                          <td>
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="G201" value="G201">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="G202" value="G202">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="G203" value="G203">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="G204" value="G204">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="G205" value="G205">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="G206" value="G206">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="G207" value="G207">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="G208" value="G208">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="G209" value="G209">
      </td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <th scope="row">3</th>
                          <td>  <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="G301" value="G301">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="G302" value="G302">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="G303" value="G303">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="G304" value="G304">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="G305" value="G305">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="G306" value="G306">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="G307" value="G307">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="G308" value="G308">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="G309" value="G309">
    </td>
                          <td></td>
                          <td></td>
                        </tr>
                      </tbody>
                    </table>

                  </div>
                </div>

      <!-- <br> -->
      <br>
      </div>
      <div id="FBLOCK" <?php echo $thirdboys; ?>>

      
                <div class="x_panel">
                  <div class="x_title">
                   <a onmouseover="document.getElementById('imag5').style='display:block;width:auto%;height:auto;'" onmouseout="document.getElementById('imag5').style='display:none;'"> <h2>F-Block</h2>
                    </a> &nbsp;
                      <img src="images/img.jpg" id="imag5" style="display: none;" alt="ROOM IMAGE">
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <table class="table">
                      <thead>
                        <tr>
                          <th>Floor:</th>
                          <th>ROOMS</th>
                          <th>S</th>
                          <th>Washroom</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <th scope="row">1</th>
                          <td><input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="F101" value="F101">
      <input type="button" class="btn btn-default" onclick="select1(this,this.value)" id="F102" value="F102">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="F103" value="F103">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="F104" value="F104">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="F105" value="F105">
      <input type="button" class="btn btn-default"   onclick="select1(this,this.value)" id="F106" value="F106">

      </td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <th scope="row">2</th>
                          <td>
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="F201" value="F201">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="F202" value="F202">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="F203" value="F203">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="F204" value="F204">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="F205" value="F205">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="F206" value="F206">
      </td>
                          <td></td>
                          <td></td>
                        </tr>
                        
                      </tbody>
                    </table>

                  </div>
                </div>

      <!-- <br> -->
      <br>
      </div>
    <div id="IBLOCK" <?php echo $secondboys; ?>>

      
                <div class="x_panel">
                  <div class="x_title">
                   <a onmouseover="document.getElementById('imag6').style='display:block;width:auto%;height:auto;'" onmouseout="document.getElementById('imag6').style='display:none;'"> <h2>I-Block</h2>
                    </a> &nbsp;
                      <img src="images/img.jpg" id="imag6" style="display: none;" alt="ROOM IMAGE">
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <table class="table">
                      <thead>
                        <tr>
                          <th style="width:40%;max-width: 40%;"></th>
                          <th style="width:40%;max-width: 40%;">ROOMS</th>
                          <th>S</th>
                          <th>Washroom</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td><input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I106" value="I106">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I107" value="I107">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I108" value="I108">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I109" value="I109">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I110" value="I110">
                            
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I105" value="I105">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I104" value="I104">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I103" value="I103">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I102" value="I102">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I101" value="I101">
                          </td>
                          <td>
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I111" value="I111">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I112" value="I112">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I113" value="I113">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I114" value="I114">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I115" value="I115">
                            
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I120" value="I120">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I119" value="I119">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I118" value="I118">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I117" value="I117">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I116" value="I116">
                          </td>
                        </tr>
                         <tr>
                          <td><input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I206" value="I206">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I207" value="I207">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I208" value="I208">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I209" value="I209">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I210" value="I210">
                            
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I205" value="I205">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I204" value="I204">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I203" value="I203">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I202" value="I202">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I201" value="I201">
                          </td>
                          <td>
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I211" value="I211">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I212" value="I212">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I213" value="I213">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I214" value="I214">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I215" value="I215">
                            
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I220" value="I220">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I219" value="I219">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I218" value="I218">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I217" value="I217">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I216" value="I216">
                          </td>
                        </tr>
                          <tr>
                          <td><input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I306" value="I306">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I307" value="I307">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I308" value="I308">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I309" value="I309">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I310" value="I310">
                            
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I305" value="I305">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I304" value="I304">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I303" value="I303">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I302" value="I302">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I301" value="I301">
                          </td>
                          <td>
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I311" value="I311">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I312" value="I312">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I313" value="I313">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I314" value="I314">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I315" value="I315">
                            
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I320" value="I320">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I319" value="I319">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I318" value="I318">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I317" value="I317">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I316" value="I316">
                          </td>
                        </tr>
                         <tr>
                          <td><input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I406" value="I406">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I407" value="I407">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I408" value="I408">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I409" value="I409">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I410" value="I410">
                            
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I405" value="I405">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I404" value="I404">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I403" value="I403">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I402" value="I402">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I401" value="I401">
                          </td>
                          <td>
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I411" value="I411">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I412" value="I412">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I413" value="I413">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I414" value="I414">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I415" value="I415">
                            
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I420" value="I420">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I419" value="I419">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I418" value="I418">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I417" value="I417">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="I416" value="I416">
                          </td>
                        </tr>
                        </tbody>
                    </table>

                  </div>
                </div>


                <!--  -->
<!-- 
                <div id="ABLOCK" <?php //echo $firstgirls; ?>>

      
                <div class="x_panel">
                  <div class="x_title">
                    <h2>A-Block</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <table class="table">
                      <thead>
                        <tr>
                          <th>Floor:</th>
                          <th>ROOMS</th>
                          <th>S</th>
                          <th>Washroom</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <th scope="row">1</th>
                          <td><input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="E101" value="E101">
      <input  type="button" onclick="select1(this,this.value)" id="E102" value="E102">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="E103" value="E103">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="E104" value="E104">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="E105" value="E105">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="E106" value="E106">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="E107" value="E107">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="E108" value="E108">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="E109" value="E109">
      </td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <th scope="row">2</th>
                          <td>
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="E201" value="E201">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="E202" value="E202">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="E203" value="E203">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="E204" value="E204">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="E205" value="E205">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="E206" value="E206">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="E207" value="E207">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="E208" value="E208">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="E209" value="E209">
      </td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <th scope="row">3</th>
                          <td>  <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="E301" value="E301">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="E302" value="E302">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="E303" value="E303">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="E304" value="E304">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="E305" value="E305">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="E306" value="E306">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="E307" value="E307">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="E308" value="E308">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="E309" value="E309">
    </td>
                          <td></td>
                          <td></td>
                        </tr>
                      </tbody>
                    </table>

                  </div>
                </div>
-->
      <br>
      </div>
 

<div id="ABLOCK" <?php echo $firstgirls; ?>>

      
                <div class="x_panel">
                  <div class="x_title">
                   <a onmouseover="document.getElementById('imag7').style='display:block;width:auto%;height:auto;'" onmouseout="document.getElementById('imag7').style='display:none;'"> <h2>A-Block</h2>
                    </a> &nbsp;
                      <img src="images/img.jpg" id="imag7" style="display: none;" alt="ROOM IMAGE">
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <table class="table">
                      <thead>
                        <tr>
                          <th>Floor:</th>
                          <th>ROOMS</th>
                          <th>S</th>
                          <th>W</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <th scope="row">1</th>
                          <td><input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="A101" value="A101">
      <input type="button" class="btn btn-default" onclick="select1(this,this.value)" id="A102" value="A102">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="A103" value="A103">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="A104" value="A104">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="A105" value="A105">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="A106" value="A106">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="A107" value="A107">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="A108" value="A108">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="A109" value="A109">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="A110" value="A110">
      </td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <th scope="row">2</th>
                          <td>
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="A201" value="A201">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="A202" value="A202">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="A203" value="A203">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="A204" value="A204">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="A205" value="A205">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="A206" value="A206">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="A207" value="A207">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="A208" value="A208">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="A209" value="A209">
     <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="A210" value="A210">
      </td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <th scope="row">3</th>
                          <td>  <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="A301" value="A301">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="A302" value="A302">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="A303" value="A303">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="A304" value="A304">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="A305" value="A305">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="A306" value="A306">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="A307" value="A307">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="A308" value="A308">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="A309" value="A309">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="A310" value="A310">
    </td>
                          <td></td>
                          <td></td>
                        </tr>
                      </tbody>
                    </table>

                  </div>
                </div>

      <!-- <br> -->
      <br>
      </div>


      <div id="BBLOCK" <?php echo $firstgirls; ?>>

      
                <div class="x_panel">
                  <div class="x_title">
                   <a onmouseover="document.getElementById('imag8').style='display:block;width:auto%;height:auto;'" onmouseout="document.getElementById('imag8').style='display:none;'"> <h2>B-Block</h2>
                    </a> &nbsp;
                      <img src="images/img.jpg" id="imag8" style="display: none;" alt="ROOM IMAGE">
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <table class="table">
                      <thead>
                        <tr>
                          <th>Floor:</th>
                          <th>ROOMS</th>
                          <th>S</th>
                          <th>W</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <th scope="row">1</th>
                          <td><input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="B101" value="B101">
      <input type="button" class="btn btn-default" onclick="select1(this,this.value)" id="B102" value="B102">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="B103" value="B103">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="B104" value="B104">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="B105" value="B105">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="B106" value="B106">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="B107" value="B107">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="B108" value="B108">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="B109" value="B109">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="B110" value="B110">
      </td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <th scope="row">2</th>
                          <td>
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="B201" value="B201">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="B202" value="B202">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="B203" value="B203">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="B204" value="B204">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="B205" value="B205">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="B206" value="B206">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="B207" value="B207">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="B208" value="B208">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="B209" value="B209">
     <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="B210" value="B210">
      </td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <th scope="row">3</th>
                          <td>  <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="B301" value="B301">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="B302" value="B302">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="B303" value="B303">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="B304" value="B304">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="B305" value="B305">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="B306" value="B306">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="B307" value="B307">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="B308" value="B308">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="B309" value="B309">
      <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="B310" value="B310">
    </td>
                          <td></td>
                          <td></td>
                        </tr>
                      </tbody>
                    </table>

                  </div>
                </div>

      <!-- <br> -->
      <br>
      </div>






    <div id="G3" <?php echo $thirdgirls; ?>>

      
                <div class="x_panel">
                  <div class="x_title">
                   <a onmouseover="document.getElementById('imag9').style='display:block;width:auto%;height:auto;'" onmouseout="document.getElementById('imag9').style='display:none;'"> <h2>GHB THIRD YEAR</h2>
                    </a> &nbsp;
                      <img src="images/img.jpg" id="imag9" style="display: none;" alt="ROOM IMAGE">
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <table class="table">
                      <thead>
                        <tr>
                          <th></th>
                          <th>ROOMS</th>
                          <th>S</th>
                          <th>Washroom</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                         <td><input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH406" value="GH406">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH407" value="GH407">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH408" value="GH408">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH409" value="GH409">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH410" value="GH410">
                            
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH405" value="GH405">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH404" value="GH404">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH403" value="GH403">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH402" value="GH402">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH401" value="GH401">
                          </td>
                          <td>
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH411" value="GH411">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH412" value="GH412">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH413" value="GH413">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH414" value="GH414">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH415" value="GH415">
                            
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH420" value="GH420">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH419" value="GH419">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH418" value="GH418">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH417" value="GH417">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH416" value="GH416">
                          </td>
                        </tr>
                         <tr>
                          <td><input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH506" value="GH506">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH507" value="GH507">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH508" value="GH508">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH509" value="GH509">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH510" value="GH510">
                            
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH505" value="GH505">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH504" value="GH504">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH503" value="GH503">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH502" value="GH502">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH501" value="GH501">
                          </td>
                          <td>
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH511" value="GH511">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH512" value="GH512">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH513" value="GH513">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH514" value="GH514">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH515" value="GH515">
                            
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH520" value="GH520">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH519" value="GH519">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH518" value="GH518">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH517" value="GH517">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH516" value="GH516">
                          </td>
                        </tr>
                          <tr>
                          <td><input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH606" value="GH606">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH607" value="GH607">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH608" value="GH608">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH609" value="GH609">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH610" value="GH610">
                            
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH605" value="GH605">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH604" value="GH604">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH603" value="GH603">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH602" value="GH602">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH601" value="GH601">
                          </td>
                          <td>
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH611" value="GH611">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH612" value="GH612">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH613" value="GH613">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH614" value="GH614">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH615" value="GH615">
                            
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH620" value="GH620">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH619" value="GH619">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH618" value="GH618">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH617" value="GH617">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH616" value="GH616">
                          </td>
                        </tr>
                         <tr>
                          <td><input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH706" value="GH706">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH707" value="GH707">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH708" value="GH708">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH709" value="GH709">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH710" value="GH710">
                            
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH705" value="GH705">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH704" value="GH704">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH703" value="GH703">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH702" value="GH702">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH701" value="GH701">
                          </td>
                          <td>
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH711" value="GH711">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH712" value="GH712">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH713" value="GH713">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH714" value="GH714">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH715" value="GH715">
                            
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH720" value="GH720">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH719" value="GH719">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH718" value="GH718">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH717" value="GH717">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH716" value="GH716">
                          </td>
                        </tr>
                        </tbody>
                    </table>

                  </div>
                </div>
    </div>
<div id="G2" <?php echo $secondgirls; ?>>

      
                <div class="x_panel">
                  <div class="x_title">
                  <a onmouseover="document.getElementById('imag10').style='display:block;width:auto%;height:auto;'" onmouseout="document.getElementById('imag10').style='display:none;'">  <h2>GHB SECOND YEAR</h2>
                    </a> &nbsp;
                      <img src="images/img.jpg" id="imag10" style="display: none;" alt="ROOM IMAGE">
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <table class="table">
                      <thead>
                        <tr>
                          <th></th>
                          <th>ROOMS</th>
                          <th>S</th>
                          <th>Washroom</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                            <td><input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH106" value="GH106">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH107" value="GH107">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH108" value="GH108">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH109" value="GH109">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH110" value="GH110">
                            
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH105" value="GH105">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH104" value="GH104">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH103" value="GH103">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH102" value="GH102">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH101" value="GH101">
                          </td>
                          <td>
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH111" value="GH111">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH112" value="GH112">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH113" value="GH113">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH114" value="GH114">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH115" value="GH115">
                            
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH120" value="GH120">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH119" value="GH119">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH118" value="GH118">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH117" value="GH117">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH116" value="GH116">
                          </td>
                        </tr>
                         <tr>
                          <td><input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH206" value="GH206">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH207" value="GH207">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH208" value="GH208">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH209" value="GH209">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH210" value="GH210">
                            
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH205" value="GH205">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH204" value="GH204">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH203" value="GH203">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH202" value="GH202">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH201" value="GH201">
                          </td>
                          <td>
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH211" value="GH211">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH212" value="GH212">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH213" value="GH213">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH214" value="GH214">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH215" value="GH215">
                            
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH220" value="GH220">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH219" value="GH219">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH218" value="GH218">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH217" value="GH217">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH216" value="GH216">
                          </td>
                        </tr>
                          <tr>
                          <td><input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH306" value="GH306">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH307" value="GH307">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH308" value="GH308">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH309" value="GH309">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH310" value="GH310">
                            
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH305" value="GH305">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH304" value="GH304">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH303" value="GH303">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH302" value="GH302">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH301" value="GH301">
                          </td>
                          <td>
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH311" value="GH311">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH312" value="GH312">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH313" value="GH313">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH314" value="GH314">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH315" value="GH315">
                            
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH320" value="GH320">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH319" value="GH319">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH318" value="GH318">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH317" value="GH317">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH316" value="GH316">
                          </td>
                        </tr>
                         
                        </tbody>
                    </table>

                  </div>
                </div>
              </div>
                <div id="G4" <?php echo $fourthgirls; ?>>

      
                <div class="x_panel">
                  <div class="x_title">
                    <!-- 
                    <a onmouseover="document.getElementById('imag11').style='display:block; width:10%;'" onmouseout="document.getElementById('imag11').style='display:none;'"> -->
                      
                      <a onmouseover="document.getElementById('imag11').style='display:block;width:auto%;height:auto;'" onmouseout="document.getElementById('imag11').style='display:none;'">
                      <h2>GHB Fourth YEAR</h2></a> &nbsp;
                      <img src="images/img.jpg" id="imag11" style="display: none;" alt="ROOM IMAGE">
                    
                    

                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <table class="table">
                      <thead>
                        <tr>
                          <th></th>
                          <th>ROOMS</th>
                          <th>S</th>
                          <th>Washroom</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                         <td>

                            
                              
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH806" value="GH806">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH807" value="GH807">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH808" value="GH808">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH809" value="GH809">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH810" value="GH810">
                            
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH805" value="GH805">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH804" value="GH804">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH803" value="GH803">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH802" value="GH802">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH801" value="GH801">
                          </td>
                          <td>
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH811" value="GH811">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH812" value="GH812">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH813" value="GH813">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH814" value="GH814">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH815" value="GH815">
                            
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH820" value="GH820">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH819" value="GH819">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH818" value="GH818">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH817" value="GH817">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH816" value="GH816">
                          </td>
                        </tr>
                         <tr>
                          <td><input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH906" value="GH906">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH907" value="GH907">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH908" value="GH908">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH909" value="GH909">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH910" value="GH910">
                            
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH905" value="GH905">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH904" value="GH904">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH903" value="GH903">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH902" value="GH902">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH901" value="GH901">
                          </td>
                          <td>
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH911" value="GH911">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH912" value="GH912">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH913" value="GH913">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH914" value="GH914">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH915" value="GH915">
                            <!-- <br><br> -->
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH920" value="GH920">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH919" value="GH919">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH918" value="GH918">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH917" value="GH917">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH916" value="GH916">
                          </td>
                        </tr>
                          <tr>
                          <td><input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH1106" value="GH1106">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH1107" value="GH1107">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH1108" value="GH1108">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH1109" value="GH1109">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH1110" value="GH1110">
                          
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH1105" value="GH1105">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH1104" value="GH1104">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH1103" value="GH1103">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH1102" value="GH1102">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH1101" value="GH1101">
                          </td>
                          <td>
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH1111" value="GH1111">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH1112" value="GH1112">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH1113" value="GH1113">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH1114" value="GH1114">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH1115" value="GH1115">
                          
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH1120" value="GH1120">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH1119" value="GH1119">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH1118" value="GH1118">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH1117" value="GH1117">
                            <input type="button" class="btn btn-default"  onclick="select1(this,this.value)" id="GH1116" value="GH1116">
                          </td>
                        </tr>
                         
                        </tbody>
                    </table>

                  </div>
                </div>
                </div>  
                <button type="button" id="modal_button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg" style="display: none;">Large modal</button>

                  <div class="modal fade bs-example-modal-lg" id ="myModal" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                          </button>
                          <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                        </div>
                        <div class="modal-body">
                          <h4>Rules</h4>
                          <p>
                            <!-- <img src="images/cropper.jpg"  class="img-responsive" style="max-height: 400px;max-width: 400px; float:right;" alt="ERROR"> -->
                            <br>
                            1)Click on room number tile to select the room you wish.<br>
                            2)You can provide multiple preferences.<br>
                            3)Choice is priotize by the sequence you choose rooms.<br>
                            4)First select is first priority<br>
                            5)Click Reset if you wish to start again.<br>
                            6)Click Save to submit preferences.
                          

                          </p>
                        </div>
                        <div class="modal-footer">
                          <br><br><button onclick="on();" type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                        </div>

                      </div>
                    </div>
                  </div>
<div id="overlay" onclick="off()">
  <div id="text"><img alt="IMAGE" class="img-responsive" src="images/All.png" width="100%" height="100%"></div>
</div>

<div style="padding:20px">
  <!-- <h2>Overlay with Text</h2> -->
  <!-- <button onclick="on()">Turn on overlay effect</button> -->
</div>

<script>
function on() {
    document.getElementById("overlay").style.display = "block";
    /*alert('Your predicted rooms are F203,E-208,E-308,E-205,G-206');*/
}

function off() {
    document.getElementById("overlay").style.display = "none";
}
</script>
       <form action="#" method="POST" id="prefForm"> 
        <input type="hidden" name="pref1" id="pref1"/>
        <input type="button" class="btn btn-default"  value="SAVE" onclick="submit_pref();">
      <!-- </form> -->
      <input type="button" class="btn btn-default"  value="RESET" onclick="reset1();">        
      
      </form>

</div>
<div class="col-md-3 col-sm-3 col-xs-12 form-group pull-right top_search">
    


</div>
      <footer>
        <div class="pull-right">
          COEP HOSTEL ADMISSION PORTAL
        </div>
        <div class="clearfix"></div>
      </footer>
      <!-- ============================ -->
        <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="../vendors/iCheck/icheck.min.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="../vendors/moment/min/moment.min.js"></script>
    <script src="../vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
    <!-- bootstrap-wysiwyg -->
    <script src="../vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js"></script>
    <script src="../vendors/jquery.hotkeys/jquery.hotkeys.js"></script>
    <script src="../vendors/google-code-prettify/src/prettify.js"></script>
    <!-- jQuery Tags Input -->
    <script src="../vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>
    <!-- Switchery -->
    <script src="../vendors/switchery/dist/switchery.min.js"></script>
    <!-- Select2 -->
    <script src="../vendors/select2/dist/js/select2.full.min.js"></script>
    <!-- Parsley -->
    <script src="../vendors/parsleyjs/dist/parsley.min.js"></script>
    <!-- Autosize -->
    <script src="../vendors/autosize/dist/autosize.min.js"></script>
    <!-- jQuery autocomplete -->
    <script src="../vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js"></script>
    <!-- starrr -->
    <script src="../vendors/starrr/dist/starrr.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>
  </body>
</html>