#!/usr/bin/python
import sys
import glob, os
from os import path
import shutil
import time
import pytesseract
from PIL import Image, ImageEnhance, ImageFilter,ExifTags
from PIL import Image, ImageFile
ImageFile.LOAD_TRUNCATED_IMAGES = True
#get the arguments passed
argList = sys.argv
year = argList[2]
branch = argList[3]
name = argList[4]
surname = argList[5]
if(argList[2] == '1'):
	year = 'fy'
elif(argList[2] == '2'):
	year = 'sy'
elif(argList[2] == '3'):
	year = 'ty'
else:
	year = 'btech'
path = '/opt/lampp/htdocs/hostel_admission/upload/'+year+'/'+branch+'/'+argList[1]+'/'

os.chdir(path)
maincount = 0
successcount = 0
failurecount = 0
text = ""
exif = {}
errorflag = 0
for file in glob.glob("*.*"):
    print(file)
    filepath  = path+str(file)
    print(filepath)
    im = Image.open(file)
    try:
        text = pytesseract.image_to_string(Image.open(file))
        print('text : ',text,'\n\n', name)
        try:
            exif = { ExifTags.TAGS[k]: v for k, v in im._getexif().items() if k in ExifTags.TAGS }
            print(exif)
        except Exception as e:
            print('first',e)
            errorflag = 1
            exif['Software'] = 'Null'
            # print(exif)
        print('name  : ',name,'\n')
        if(name.lower() in text.lower() or surname.lower() in text.lower()):
            successcount += 1
            print('in here')
        elif(name in text):
            successcount += 1
        elif(text.strip() == ""):
            failurecount += 1
        else:
            failurecount += 1
    except Exception as e:
        print('second',e)
        errorflag = 1
        time.sleep(5)
    maincount += 1
print(successcount, failurecount)
if(errorflag == 1):
    print('error')
    # sys.exit(1)
if(successcount > failurecount):
    print('here success')
    sys.exit(1)
elif(successcount == failurecount):
    print('failure')
    sys.exit(0)
else:
    print('failure')
    sys.exit(0)