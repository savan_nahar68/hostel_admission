<!DOCTYPE html>
<?php
error_reporting(0); 
include('../../auth.php');
require_once('../../connection.php');
$mis = $_SESSION['mis'];
$sql = "SELECT * FROM tabs";
$result = $conn->query($sql);
$data = $result->fetch_assoc();
if(isset($data['openform']) && $data['openform'] == '0') {
  $openform = 'style="pointer-events: none;opacity: 0.4;"';
}
if(isset($data['openlist']) && $data['openlist'] == '0') {
  $openlist = 'style="pointer-events: none;opacity: 0.4;"';
}
if(isset($data['openroomallocation']) && $data['openroomallocation'] == '0') {
  $openroomallocation = 'style="pointer-events: none;opacity: 0.4;"';
}
if(isset($data['openroomlist']) && $data['openroomlist'] == '0') {
  $openroomlist = 'style="pointer-events: none;opacity: 0.4;"';
}
  
  
?>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>COEP | HOSTEL ADMISSION</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- bootstrap-wysiwyg -->
    <link href="../vendors/google-code-prettify/bin/prettify.min.css" rel="stylesheet">
    <!-- Select2 -->  
    <link href="../vendors/select2/dist/css/select2.min.css" rel="stylesheet">
    <!-- Switchery -->
    <link href="../vendors/switchery/dist/switchery.min.css" rel="stylesheet">
    <!-- starrr -->
    <link href="../vendors/starrr/dist/starrr.css" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="../vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.php" class="site_title"><i class="fa fa-building-o " style="color : cyan;"></i> <span>COEP HOSTEL</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="images/user.png" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2><?php if(isset($_SESSION['name'])) echo $_SESSION['name']; else echo 'Admin'; ?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                  <li><a href="index.php"><i class="fa fa-home"></i> Home</a>
                  </li>
                  <li <?php if(isset($openform)) echo $openform;?>><a><i class="fa fa-edit"></i> Forms <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                    <li><a href="admissionform.php"><?php if(!isset($val) || $val < 1) echo "Admission form"; else echo "Form Status";?></a></li>
                    </ul>
                  </li>
                  <li <?php if(isset($openlist)) echo $openlist;?>><a><i class="fa fa-building"></i>Allotment List<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                    <li><a href="seatdisplayaccepted.php"><i class="fa fa-check"></i>Alloted List</a></li>
                    <li><a href=seatdisplaywaiting.php><i class="fa fa-clock-o"></i>Waiting List</a></li>
                    </ul>
                  </li>
                  <li <?php if(isset($openroomallocation)) echo $openroomallocation;?>><a href="invoice.php"><i class="fa fa-credit-card-alt"></i>Fees Payment</span></a>
                  </li>
                  <li <?php if(isset($openroomlist)) echo $openroomlist;?>><a href="roomlist.php"><i class="fa fa-file-pdf-o"></i>Room Allotment List</span></a>
                  </li>
                </ul>
              </div>

            </div>
            
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="images/user.png" alt=""><?php if(isset($_SESSION['name'])) echo $_SESSION['name']; else echo 'Admin'; ?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    
                    <li><a href="logout.php"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>

                <li role="presentation" class="dropdown">
                  <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                    
                  </a>
                  
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- <div class=""> -->
       <div class="right_col" role="main" style="min-height : 800px">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Room Allotment list</h3>
              </div>
            </div>

          </div><br><br>
         <div class="x_content">


                  
                      <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="embed-responsive embed-responsive-16by9">
                          <iframe src="roomlistboys.php" class="embed responsive-item"></iframe>
                         </div>
                      </div>
                   
                   
                  
                

<!-- Modal -->
        <div class="modal fade" id="myModal" role="dialog">
          <div class="modal-dialog modal-md">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Documents</h4>
              </div>
              <div class="modal-body">
                <div class="row">
                  <div class="column" style="background-color:#aaa;">
                    <center><img src="images/img.jpg"></center>
                  </div>
                  <div class="column" style="background-color:#bbb;">
                    <center><img src="images/img.jpg"></center>
                  </div>
                </div>

                <div class="row">
                  <div class="column" style="background-color:#ccc;">
                    <center><img src="images/img.jpg"></center>
                  </div>
                  <div class="column" style="background-color:#ddd;">
                    <center><img src="images/img.jpg"></center>
                  </div>
                </div>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>

          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
       
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            COEP HOSTEL ADMISSION PORTAL
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>
   <script type="text/javascript">
          
          var dropdown = " <span class='caret'></span>";
          function enable1(year) {
            document.getElementById('btn-branch').disabled = false;
            document.getElementById('btn-year').innerHTML = year + dropdown;
            $('input[name=year]').val(year);
          }
          function enable2(branch) {
            document.getElementById('btn-caste').disabled = false;
            document.getElementById('btn-branch').innerHTML = branch + dropdown;
            $('input[name=branch]').val(branch);
          }
          function showCaste(caste) {
            document.getElementById('btn-gender').disabled = false;
            document.getElementById('btn-caste').innerHTML = caste + dropdown;
            $('input[name=caste]').val(caste);
          }
          function showme(){
            document.getElementById('datatableshow').style = 'display:block;';
            document.getElementById('submitbutton').style = 'display:block;';
          }
          function showgender(gender) {
             document.getElementById('btn-gender').innerHTML = gender + dropdown;
            $('input[name=gender]').val(gender);
          }
        </script>
          <script>
          windows.onload('gridView()');
        // Get the elements with class="column"
        var elements = document.getElementsByClassName("column");

        // Declare a loop variable
        var i;

          for (i = 0; i < elements.length; i++) {
            elements[i].style.width = "50%";
          }

        /* Optional: Add active class to the current button (highlight it) */
        var container = document.getElementById("btnContainer");
        var btns = container.getElementsByClassName("btn");
        for (var i = 0; i < btns.length; i++) {
          btns[i].addEventListener("click", function(){
            var current = document.getElementsByClassName("active");
            current[0].className = current[0].className.replace(" active", "");
            this.className += " active";
          });
        }
        </script>
      
    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="../vendors/iCheck/icheck.min.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="../vendors/moment/min/moment.min.js"></script>
    <script src="../vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
    <!-- bootstrap-wysiwyg -->
    <script src="../vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js"></script>
    <script src="../vendors/jquery.hotkeys/jquery.hotkeys.js"></script>
    <script src="../vendors/google-code-prettify/src/prettify.js"></script>
    <!-- jQuery Tags Input -->
    <script src="../vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>
    <!-- Switchery -->
    <script src="../vendors/switchery/dist/switchery.min.js"></script>
    <!-- Select2 -->
    <script src="../vendors/select2/dist/js/select2.full.min.js"></script>
    <!-- Parsley -->
    <script src="../vendors/parsleyjs/dist/parsley.min.js"></script>
    <!-- Autosize -->
    <script src="../vendors/autosize/dist/autosize.min.js"></script>
    <!-- jQuery autocomplete -->
    <script src="../vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js"></script>
    <!-- starrr -->
    <script src="../vendors/starrr/dist/starrr.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>
  
  </body>
</html>
