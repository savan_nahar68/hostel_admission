import numpy as np
import pandas as pd
from sklearn.utils import shuffle
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.ensemble import RandomForestClassifier
import pickle
import sys

argList = sys.argv
cgpa = argList[1]
branch = argList[2]
if(branch == 'comp'):
	branch = 'COMPUTER'
elif(branch == 'it'):
	branch = 'IT'
elif(branch == 'electrical'):
	branch = 'ELECTRICAL'
elif(branch == 'entc'):
	branch = 'ENTC'
elif(branch == 'instru'):
	branch = 'INSTRU'
elif(branch == 'meta'):
	branch = 'META'
elif(branch == 'mech'):
	branch = 'MECHANICAL'
elif(branch == 'planning'):
	branch = 'PLANNING'
elif(branch == 'prod'):
	branch = 'PROD'
elif(branch == 'civil'):
	branch = 'CIVIL'


string = cgpa+','+branch+'\n'
with open('test.csv', 'a') as f:
	f.write(string)
loaded_model = pickle.load(open('finalized_model.sav', 'rb'))
test = pd.read_csv("test.csv");
test = pd.get_dummies(test)
#print(test)
result = loaded_model.predict(test)
#print(result[result.shape[0]-1:, :][0][0])
result[result.shape[0]-1:, :][0][0].astype(np.int64)

if(result[result.shape[0]-1:, :][0][0].astype(np.int64)):
	sys.exit(0);
elif(result[result.shape[0]-1:, :][0][1].astype(np.int64)):
	sys.exit(1);
elif(result[result.shape[0]-1:, :][0][2].astype(np.int64)):
	sys.exit(2);
