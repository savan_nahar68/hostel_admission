<?php
error_reporting(0); 
 include('../../auth.php');
require_once('../../connection.php');
$mis = $_SESSION['mis'];
$sql = "SELECT * FROM tabs";
$result = $conn->query($sql);
$data = $result->fetch_assoc();
if(isset($data['openform']) && $data['openform'] == '0') {
  $openform = 'style="pointer-events: none;opacity: 0.4;"';
}
if(isset($data['openlist']) && $data['openlist'] == '0') {
  $openlist = 'style="pointer-events: none;opacity: 0.4;"';
}
if(isset($data['openroomallocation']) && $data['openroomallocation'] == '0') {
  $openroomallocation = 'style="pointer-events: none;opacity: 0.4;"';
}
if(isset($data['openroomlist']) && $data['openroomlist'] == '0') {
  $openroomlist = 'style="pointer-events: none;opacity: 0.4;"';
}
$sql = "select * from student where mis = '$mis'";
$result = $conn->query($sql);
// echo $result->num_rows;
if($result->num_rows) {
  $row = $result->fetch_assoc();
  $name = $row['fname'].' '.$row['lname'] ;
  $val = $row['accept'];
  $name_one = array("disabled","disabled","disabled","disabled","disabled");
  $form1 = "style='display:block;'";
  $prog = "style='display:none;'";
    if($val > 0) {
  $form1 = "style='display:none;'";
  $prog = "style='display:block;'";
  if($val != 5) {
    for($i = 0; $i < $val;$i++) {
      $name_one[$i] = "done";
    }
    $name_one[$i] = "selected";
  }
  else {
    $name_one = array("done","done","done","done","done");
  }
  }
  $i = 0;
  $fname = $row['fname'];
  $mname = $row['lname'];
  $lname = $row['mname'];
  // echo explode(',',$row['address'],-1);
  $address = implode(',',explode(',',$row['address'],-1));
  $pincode = array_pop(explode(',',$row['address']));
  // $index=$row['year'];
  // $year = $row[(int)$_POST['year']];
  // $branch = $row['branch'];
  // $cgpa = $row['cgpa'];
  $mail = $row['mail'];
  $mobile = $row['mobile'];
  $pphone = $row['pphone'];
  $category = $row['category'];
  // $income= $row['income'];
  if($row['sbc']) $sbc = 'checked';
  else $sbc = "";
  if($row['pwd']) $pwd = 'checked';
  else $pwd = "";
  if($row['gender']) $gender = 'checked';
  else $gender = "";
  if($row['nri']) $nri = 'checked';
  else $nri = "";
  //  $row['pwd'];
  // $gender = $row['gender'];
  $bgroup = $row['bgroup'];
  // echo $bgroup;
  //$documents = $_POST['first-name'];
  // $nri = $row['nri'];
}
$noDisplay = "style='display:none;'";
if( isset($val) && $val != 0) {
  $displayProg = "style='display:block;'";
  $displayForm = "style='display:none;'";
}
else {
  $displayForm = "style='display:block;'";
  $displayProg = "style='display:none;'";
}
?>


  




<!DOCTYPE html> 
<html lang="en">
  <head>
    <script type="text/javascript">
     function openModal1() {
       if(document.getElementById('myModal').style.display != 'none')
        setTimeout('document.getElementById("modal_button").click();',1000)
        
      }
    </script>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>COEP | HOSTEL ADMISSION</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="../vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <!-- bootstrap-datetimepicker -->
    <link href="../vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" rel="stylesheet">
    <!-- Ion.RangeSlider -->
    <link href="../vendors/normalize-css/normalize.css" rel="stylesheet">
    <link href="../vendors/ion.rangeSlider/css/ion.rangeSlider.css" rel="stylesheet">
    <link href="../vendors/ion.rangeSlider/css/ion.rangeSlider.skinFlat.css" rel="stylesheet">
    <!-- Bootstrap Colorpicker -->
    <link href="../vendors/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css" rel="stylesheet">

    <link href="../vendors/cropper/dist/cropper.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="nav-md" onload="openModal1()">
    <div class="container body">
      <div class="main_container">
      <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.php" class="site_title"><i class="fa fa-paw"></i> <span>COEP HOSTEL</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="images/user.png" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2><?php if(isset($name)) echo $name; ?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                  <li><a href="index.php"><i class="fa fa-home"></i> Home</a>
                  </li>
                  <li <?php if(isset($openform)) echo $openform;?>><a><i class="fa fa-edit"></i> Forms <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                    <li><a href="admissionform.php"><?php if(!isset($val) || $val < 1) echo "Admission form"; else echo "Form Status";?></a></li>
                    </ul>
                  </li>
                  <li <?php if(isset($openlist)) echo $openlist;?>><a><i class="fa fa-building"></i>Allotment List<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                    <li><a href="seatdisplayaccepted.php"><i class="fa fa-check"></i>Alloted List</a></li>
                    <li><a href=seatdisplaywaiting.php><i class="fa fa-clock-o"></i>Waiting List</a></li>
                    </ul>
                  </li>
                  <li <?php if(isset($openroomallocation)) echo $openroomallocation;?>><a href="invoice.php"><i class="fa fa-credit-card-alt"></i>Fees Payment</span></a>
                  </li>
                  <li <?php if(isset($openroomlist)) echo $openroomlist;?>><a href="roomlist.php"><i class="fa fa-file-pdf-o"></i>Room Allotment List</span></a>
                  </li>
                </ul>
              </div>

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
           <!--  <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div> -->
            <!-- /menu footer buttons -->
          </div>
        </div>
        
        <!-- top navigation -->
        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="images/user.png" alt=""><?php if(isset($name)) echo $name; ?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    
                    <li><a href="logout.php"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>

                <li role="presentation" class="dropdown">
                  <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                    
                  </a>
                  
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Hostel Admission Form</h3>
              </div>
            </div>
            <div class="clearfix"></div>

            <div class="row"  >
              <div class="col-md-12 col-sm-12 col-xs-12" <?php echo $displayForm;?>>
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Fill the following information </h2>
                    <ul class="nav navbar-right panel_toolbox">
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <!-- Smart Wizard -->
                    <div id="wizard" class="form_wizard wizard_horizontal" >
                      <ul class="wizard_steps">
                        <li>
                          <a href="#step-1">
                            <span class="step_no">1</span>
                            <span class="step_descr">
                              Step 1<br />
                              <small>Information Fill</small>
                            </span>
                          </a>
                        </li>
                        <li>
                          <a href="#step-2">
                            <span class="step_no">2</span>
                            <span class="step_descr">
                              Step 2<br />
                              <small>Documents Upload</small>
                            </span>
                          </a>
                        </li>
                        <li>
                          <a href="#step-3">
                            <span class="step_no">3</span>
                            <span class="step_descr">
                              Step 3<br />
                              <small>Rules and Regulations</small>
                            </span>
                          </a>
                        </li>
                      </ul>
                      
                      <form class="form-horizontal form-label-left" method="POST" action="upload.php" enctype="multipart/form-data" accept="images/*">
                        <div id="step-1">
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">First Name <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                              <input type="text" id="first-name" name="first-name"  class="form-control col-md-7 col-xs-12" value=<?php if(isset($fname)) echo $fname;?>>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Last Name <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                              <input type="text" id="last-name" name="last-name" class="form-control col-md-7 col-xs-12" value=<?php if(isset($lname)) echo $lname;?>>
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Middle Name / Initial</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                              <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="middle-name" value=<?php if(isset($mname)) echo $mname;?>>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Gender</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                              <div id="gender" class="btn-group" data-toggle="buttons">
                                <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                  <input type="radio" name="gender" value="1" <?php if(isset($gender) && $gender) echo $gender;?>> &nbsp; Male &nbsp;
                                </label>
                                <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                  <input type="radio" name="gender" value="0" <?php if(isset($gender) && !$gender) echo $gender;?>> Female
                                </label>
                              </div>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Date Of Birth <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                              <div class="form-group">
                                <div class='input-group date' id='datetimepicker3'>
                                  <input type='text' class="form-control" />
                                  <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                  </span>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Address <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                              <input id="address" name="address" class="form-control col-md-7 col-xs-12"  type="text" value="<?php if(isset($address)) echo $address;?>">
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Pincode<span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                              <input id="pincode" name="pincode" class="form-control col-md-7 col-xs-12"  type="text" value=<?php if(isset($pincode)) echo $pincode;?>>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Enrollment ID<span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                              <input id="enrollment" name="enrollment" class="form-control col-md-7 col-xs-12"  type="text" value=<?php if(isset($mis)) echo $mis;?>>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Year of Study <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                              <select class="form-control" id="year" name="year">
                                <option value="0">Choose option</option>
                                <option value="1">F.Y.</option>
                                <option value="2">S.Y.</option>
                                <option value="3">T.Y</option>
                                <option value="4">B.Tech</option>
                              </select>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Branch<span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                              <!-- <input id="branch" class="form-control col-md-7 col-xs-12"  type="text"> -->
                              <select class="form-control" id="branch" name="branch">
                                <option>Choose option</option>
                                <option value = 'civil'>civil</option><option value = 'comp'>comp</option><option value = 'electrical'>electrical</option><option value = 'entc'>entc</option><option value = 'instru'>instru</option><option value = 'it'>it</option><option value = 'mech'>mech</option><option value = 'meta'>meta</option><option value = 'planning'>planning</option><option value = 'prod'>prod</option>                            <!-- <option>Option two</option>
                                <option>Option three</option>
                                <option>Option four</option> -->
                              </select>
                              
                              <!-- Default dropright button -->
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">CGPA/Merit NO.<span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                              <input id="cgpa" name="cgpa" class="form-control col-md-7 col-xs-12"  type="text">
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Email<span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                              <input id="email" name="email" class="form-control col-md-7 col-xs-12"  type="email" value=<?php if(isset($mail)) echo $mail;?>>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Mobile Number<span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                              <input id="mob" name="mob" class="form-control col-md-7 col-xs-12"  type="text" value=<?php if(isset($mobile)) echo $mobile;?>>
                            </div>
                          </div>

                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Parent phone number<span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                              <input id="pphone" name="pphone" class="form-control col-md-7 col-xs-12"  type="text" value=<?php if(isset($pphone)) echo $pphone;?>>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Category<span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                              <select class="form-control" id="category" name="category">
                                <option value="none">Choose option</option>
                                <option value="open">OPEN</option>
                                <option value="obc">OBC</option>
                                <option value="vj/ntb">VJ/NT(B)</option>
                                <option value="ntc/ntd">NT(C)/NT(D)</option>
                                <option value="sc">SC</option>
                                <option value="st">ST</option>
                              </select>                        
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Income <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                              <input id="income" name="income" class="form-control col-md-7 col-xs-12"  type="text">
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Is SBC ?</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                              <div id="sbc" class="btn-group" data-toggle="buttons">
                                <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                  <input type="radio" name="sbc" value="1" <?php if(isset($sbc) && $sbc) echo $sbc;?>> &nbsp; YES &nbsp;
                                </label>
                                <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                  <input type="radio" name="sbc" value="0" <?php if(isset($sbc) && !$sbc) echo $sbc;?>> NO
                                </label>
                              </div>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Is PWD ?</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                              <div id="pwd" class="btn-group" data-toggle="buttons">
                                <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                  <input type="radio" name="pwd" value="1" <?php if(isset($pwd) && $pwd) echo $pwd;?>> &nbsp; YES &nbsp;
                                </label>
                                <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                  <input type="radio" name="pwd" value="0" <?php if(isset($pwd) && $pwd) echo $pwd;?>> NO
                                </label>
                              </div>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Is NRI ?</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                              <div id="nri" class="btn-group" data-toggle="buttons">
                                <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                  <input type="radio" name="nri" value="1" <?php if(isset($nri) && $nri) echo $nri;?>> &nbsp; YES &nbsp;
                                </label>
                                <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                  <input type="radio" name="nri" value="0" <?php if(isset($nri) && $nri) echo $nri;?>> NO
                                </label>
                              </div>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Blood Group<span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                              <input id="bgroup"  name = "bgroup" class="form-control col-md-7 col-xs-12"  type="text" value=<?php if(isset($bgroup)) echo $bgroup;?>>
                            </div>
                          </div>
                        </div>
                        <div id="step-2">
                          <h2 class="StepTitle">Step 2 Content</h2>
                          <table class="table table-striped">
                            <thead>
                              <tr>
                                <th>#</th>
                                <th>Document Name</th>
                                <th>Upload</th>
                                <th>Status</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <th scope="row">1</th>
                                <td>No Dues Certificate</td>
                                <td><input type="file" class="btn btn-primary" id="dues" name="dues"/></td>
                                <td></td>
                              </tr>
                              <tr>
                                <th scope="row">2</th>
                                <td>Caste Validity</td>
                                <td><input class="btn btn-primary" type="file" id="cvalidaty" name="cvalidity"/></td>
                                <td></td>
                              </tr>
                              <tr>
                                <th scope="row">3</th>
                                <td>Admission Confirmatin form</td>
                                <td><input class="btn btn-primary" type="file" id = "addConfirm" name="addConfirm"/></td>
                                <td></td> 
                              </tr>
                              <tr>
                                <th scope="row">3</th>
                                <td>Passport Photo</td>
                                <td><input class="btn btn-primary" type="file" id = "photo" name="photo"/></td>
                                <td></td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                        <div id="step-3">
                          <h2 class="StepTitle col-xs-offset-1">Rules and regulations </h2>  
                          <div class="form-group">
                            <div class="col-xs-9 col-xs-offset-1">
                              <div style="border: 1px solid #e5e5e5; height: 400px; overflow: auto; padding: 10px;">
                                <p><br>
                                1. Ragging is strictly prohibited.
The Maharashtra Legislative Council has passed a bill (MAHARASHTRA ACT No.
XXXIII OF 1999, THE MAHARASHTRA PROHIBITION OF RAGGING ACT, 1999) to
prohibit ragging in educational institutions in the State of Maharashtra on 7 th April 1999.<br>
2. Absence from Hostel: Students should take prior approval from the rector before leaving
the hostel.
<br>3. Room change without consent of hostel office will not be permitted.

Timings for Boys Hostel: 5.00am to 11.30pm & Timings for Girls Hostel: 6.00am
To 9.00pm.
<br>5. If a student damages any Hostel Property like wash basin, furniture, electrical equipment etc. same
shall be recovered from hostel deposit. Residents shall keep their rooms neat and clean. Room
furniture’s such as cot, chair, table and electrical fittings such as ceiling fan, tube light etc., are
required to be maintained by the students in good condition. If a student observes any damages or
defect in the furniture issued to him/ her or in the permanent fittings his/her duty to bring it to the
notice of the hostel office. Failing which it will be presumed that everything was in order at the time
of occupying the room.<br>
6. It is mandatory for girl students to take a written permission of girl’s hostel rector for any
absence in hostel during night hours (i.e. outstation stay). If any girl student disobeys this rule, a
heavy fine will be imposed first time and student will be immediately expelled from the hostel,
if repeated second time. Night stay in local guardians house is permitted twice in month. Night stayother than Local Guardians place will not be permitted.<br>
7. Hosteller should obey the rules set up by the committee to maintain discipline in the mess hall.
Mess advance has to be paid in the beginning; and then every month mess bill should be paid
within 5 days of announcement and thereafter Rs. 10/- per day will be charged as fine. The mess
advance will be adjusted at the end of the year. Only hostel students are permitted to avail the
facility of mess. No Guests or outsiders are permitted to take meal in the mess without prior
permission of hostel office.<Br>8.The decision of the Chief Rector shall be final on the matters pertaining to the hostel.<br>9.Visiting Timing for Girls hostel:
Morning: 08.00 am. to 10.00 am.
Evening: 06.00 pm to 08.00 pm.
(Only mother and Father can meet their ward other than visiting time)</p>
                              </div>
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="col-xs-6 col-xs-offset-1">
                              <div class="checkbox">
                                <label>
                                  <input type="checkbox" name="agree" id = "agree"/> Agree with the terms and conditions
                                </label>
                              </div>
                            </div>
                          </div>
                        </div>
                        <input type='submit' value='submit' id="submit-form" hidden/>
                      </form>
                    </div>

                    <!-- End SmartWizard Content -->
                  </div>
                </div>
              </div>
              <div class="clearfix"></div>
              <div class="row" <?php echo $displayProg;?>>
                <div class="col-md-12 col-sm-12 col-xs-12">
                  <div class="x_panel">
                    <div class="x_title">
                      <h2>Progress</h2>
                      <ul class="nav navbar-right panel_toolbox"></ul>
                      <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                      <!-- Smart Wizard -->
                      <div  id="wizard1" class="form_wizard wizard_horizontal">
                      <ul class="wizard_steps">
                        <li>
                          <a href="#Sstep-1" class= <?php echo $name_one[$i];$i++; ?> >
                            <span class="step_no">1</span>
                            <span class="step_descr">
                              Form Uploaded<br />
                              <!-- <small>Information Fill</small> -->
                            </span>
                          </a>
                        </li>
                        <li>
                          <a href="#Sstep-2" class=<?php echo $name_one[$i];$i++; ?>>
                            <span class="step_no">2</span>
                            <span class="step_descr">
                              Machine Verified<br />
                              <small>Documents are verified by machine</small>
                            </span>
                          </a>
                        </li>
                        <li>
                          <a href="#Sstep-3" class=<?php echo $name_one[$i];$i++; ?>>
                            <span class="step_no">3</span>
                            <span class="step_descr">
                              Admin Verification <br />
                              <!-- <small>Rules and Regulations</small> -->
                            </span>
                          </a>
                        </li>
                        <li>
                          <a href="#Sstep-4" class=<?php echo $name_one[$i];$i++; ?>>
                            <span class="step_no">4</span>
                            <span class="step_descr">
                              Fees Payment<br />
                              <!-- <small>Rules and Regulations</small> -->
                            </span>
                          </a>
                        </li>
                        <li>
                          <a href="#Sstep-5" class=<?php echo $name_one[$i];$i++; ?>>
                            <span class="step_no">5</span>
                            <span class="step_descr">
                              Room Allocation<br />
                              <!-- <small>Rules and Regulations</small> -->
                            </span>
                          </a>
                        </li>
                        
                      </ul>
                    </div>
                    <!-- End SmartWizard Content -->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
        <!-- /page content -->
      <button type="button" id="modal_button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg" style="display: none;">Large modal</button>

      <div class="modal fade bs-example-modal-lg" id ="myModal" tabindex="-1" role="dialog" aria-hidden="true" <?php echo $displayForm?>>
        <div class="modal-dialog modal-lg">
          <div class="modal-content">

            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
              </button>
              <h4 class="modal-title" id="myModalLabel">Admission Form</h4>
            </div>
            <div class="modal-body">
              <h4>Guidelines</h4>
              <p>
                1)Make sure you fill valid and authenticate  information.<br>
                2)Documents should be proper.<br>
                3)Documents will be tested for validity by software program as well as hostel incharge.<br>
                4)In case of fake information provided, student will be discarded from  admission process.<br>
                5)Read rules and regulations before submitting the form.<br>
              </p>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
            </div>

          </div>
        </div>
      </div>
      <!-- footer content -->
      <footer>
        <div class="pull-right">
          COEP HOSTEL ADMISSION PORTAL
        </div>
        <div class="clearfix"></div>
      </footer>
      <!-- /footer content -->
    </div>
    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- jQuery Smart Wizard -->
    <script src="../vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>

  </body>
</html>