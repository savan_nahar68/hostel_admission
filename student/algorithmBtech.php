<?php
    require('../../connection.php');
    // $boys = "CREATE VIEW boys as SELECT mis,caste,cgpa,year,branch,sbc,pwd,nri where gender=1 ORDER BY(cgpa) DESC";
    // $conn->query($boys);
    // $girls = "CREATE VIEW girls as SELECT mis,caste,cgpa,year,branch,sbc,pwd,nri where gender=0 ORDER BY(cgpa) DESC";
    // $conn->query($girls);

    // $tyBoys = "CREATE VIEW tyBoys as SELECT * from boys where year = 4 ORDER BY(cgpa) DESC";
    // $syBoys = "CREATE VIEW syBoys as SELECT * from boys where year = 3";
    // $tyBoys = "CREATE VIEW tyBoys as SELECT * from boys where year = 3";
    // $btBoys = "CREATE VIEW btBoys as SELECT * from boys where year = 4";

    // $tyGirls = "CREATE VIEW tyGirls as SELECT * from girls where year = 4";
    // $syGirls = "CREATE VIEW syGirls as SELECT * from girls where year = 3";
    // $tyGirls = "CREATE VIEW tyGirls as SELECT * from girls where year = 3";
    // $btGirls = "CREATE VIEW btGirls as SELECT * from girls where year = 4";
    
    $tyBoysMech = "SELECT  mis,category,cgpa,year,branch,sbc,pwd from STUDENT where year = 4 and gender=1 and branch = 'mech' and nri = 0 ORDER BY(cgpa) DESC";
    $result = $conn->query($tyBoysMech);
    if($result->num_rows > 0) {
        $count_open = 0;
        $sc = 6;
        $st = 3;
        $vj_ntb = 3;
        $ntc_ntd = 3;
        $obc = 9;
        while($row = $result->fetch_assoc()) {
            $mis = $row['mis'];
            $category = $row['category'];
            if($count_open < 24) {
                $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`) VALUES ('$mis',1,'$category')";
            }
            else{
                if($category == "vj/ntb" && $vj_ntb != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`) VALUES ('$mis',1,'$category')";
                    $vj_ntb -= 1;
                }
                elseif($category == "ntc/ntd" && $ntc_ntd != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`) VALUES ('$mis',1,'$category')";
                    $ntc_ntd -= 1;
                }
                elseif($category == "sc" && $sc != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`) VALUES ('$mis',1,'$category')";
                    $sc -= 1;
                }
                elseif($category == "st" && $st != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`) VALUES ('$mis',1,'$category')";
                    $st -= 1;
                }
                elseif($category == "obc" && $obc != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`) VALUES ('$mis',1,'$category')";
                    $obc -= 1;
                }
                else{
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`) VALUES ('$mis',0,'$category')";
                }
            }
            if($conn->query($insertIntoStatus)){

            }
            else{
                echo 'Error';
            }
            $count_open += 1;
        }
    }

    $tyBoysCivil = "SELECT  mis,category,cgpa,year,branch,sbc,pwd from STUDENT where year = 4 and gender=1 and branch = 'civil' and nri = 0 ORDER BY(cgpa) DESC";
    $result = $conn->query($tyBoysCivil);
    if($result->num_rows > 0) {
        $count_open = 0;
        $sc = 3;
        $st = 2;
        $vj_ntb = 1;
        $ntc_ntd = 1;
        $obc = ;
        while($row = $result->fetch_assoc()) {
            $mis = $row['mis'];
            $category = $row['category'];
            if($count_open < ) {
                $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`) VALUES ('$mis',1,'$category')";
            }
            else{
                if($category == "vj/ntb" && $vj_ntb != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`) VALUES ('$mis',1,'$category')";
                    $vj_ntb -= 1;
                }
                elseif($category == "ntc/ntd" && $ntc_ntd != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`) VALUES ('$mis',1,'$category')";
                    $ntc_ntd -= 1;
                }
                elseif($category == "sc" && $sc != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`) VALUES ('$mis',1,'$category')";
                    $sc -= 1;
                }
                elseif($category == "st" && $st != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`) VALUES ('$mis',1,'$category')";
                    $st -= 1;
                }
                elseif($category == "obc" && $obc != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`) VALUES ('$mis',1,'$category')";
                    $obc -= 1;
                }
                else{
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`) VALUES ('$mis',0,'$category')";
                }
            }
            if($conn->query($insertIntoStatus)){

            }
            else{
                echo 'Error';
            }
            $count_open += 1;
        }
    }

    $tyBoysComp = "SELECT  mis,category,cgpa,year,branch,sbc,pwd from STUDENT where year = 4 and gender=1 and branch = 'comp' and nri = 0 ORDER BY(cgpa) DESC";
    $result = $conn->query($tyBoysComp);
    if($result->num_rows > 0) {
        $count_open = 0;
        $sc = 3;
        $st = 2;
        $vj_ntb = 1;
        $ntc_ntd = 1;
        $obc = 5;
        while($row = $result->fetch_assoc()) {
            $mis = $row['mis'];
            $category = $row['category'];
            if($count_open < 12) {
                $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`) VALUES ('$mis',1,'$category')";
            }
            else{
                if($category == "vj/ntb" && $vj_ntb != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`) VALUES ('$mis',1,'$category')";
                    $vj_ntb -= 1;
                }
                elseif($category == "ntc/ntd" && $ntc_ntd != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`) VALUES ('$mis',1,'$category')";
                    $ntc_ntd -= 1;
                }
                elseif($category == "sc" && $sc != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`) VALUES ('$mis',1,'$category')";
                    $sc -= 1;
                }
                elseif($category == "st" && $st != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`) VALUES ('$mis',1,'$category')";
                    $st -= 1;
                }
                elseif($category == "obc" && $obc != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`) VALUES ('$mis',1,'$category')";
                    $obc -= 1;
                }
                else{
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`) VALUES ('$mis',0,'$category')";
                }
            }
            if($conn->query($insertIntoStatus)){

            }
            else{
                echo 'Error';
            }
            $count_open += 1;
        }
    }

    $tyBoysElect = "SELECT  mis,category,cgpa,year,branch,sbc,pwd from STUDENT where year = 4 and gender=1 and branch = 'electrical' and nri = 0 ORDER BY(cgpa) DESC";
    $result = $conn->query($tyBoysElect);
    if($result->num_rows > 0) {
        $count_open = 0;
        $sc = 3;
        $st = 2;
        $vj_ntb = 1;
        $ntc_ntd = 1;
        $obc = 5;
        while($row = $result->fetch_assoc()) {
            $mis = $row['mis'];
            $category = $row['category'];
            if($count_open < 12) {
                $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`) VALUES ('$mis',1,'$category')";
            }
            else{
                if($category == "vj/ntb" && $vj_ntb != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`) VALUES ('$mis',1,'$category')";
                    $vj_ntb -= 1;
                }
                elseif($category == "ntc/ntd" && $ntc_ntd != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`) VALUES ('$mis',1,'$category')";
                    $ntc_ntd -= 1;
                }
                elseif($category == "sc" && $sc != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`) VALUES ('$mis',1,'$category')";
                    $sc -= 1;
                }
                elseif($category == "st" && $st != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`) VALUES ('$mis',1,'$category')";
                    $st -= 1;
                }
                elseif($category == "obc" && $obc != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`) VALUES ('$mis',1,'$category')";
                    $obc -= 1;
                }
                else{
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`) VALUES ('$mis',0,'$category')";
                }
            }
            if($conn->query($insertIntoStatus)){

            }
            else{
                echo 'Error';
            }
            $count_open += 1;
        }
    }

    $tyBoysEntc = "SELECT  mis,category,cgpa,year,branch,sbc,pwd from STUDENT where year = 4 and gender=1 and branch = 'entc' and nri = 0 ORDER BY(cgpa) DESC";
    $result = $conn->query($tyBoysEntc);
    if($result->num_rows > 0) {
        $count_open = 0;
        $sc = 3;
        $st = 2;
        $vj_ntb = 1;
        $ntc_ntd = 1;
        $obc = 5;
        while($row = $result->fetch_assoc()) {
            $mis = $row['mis'];
            $category = $row['category'];
            if($count_open < 12) {
                $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`) VALUES ('$mis',1,'$category')";
            }
            else{
                if($category == "vj/ntb" && $vj_ntb != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`) VALUES ('$mis',1,'$category')";
                    $vj_ntb -= 1;
                }
                elseif($category == "ntc/ntd" && $ntc_ntd != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`) VALUES ('$mis',1,'$category')";
                    $ntc_ntd -= 1;
                }
                elseif($category == "sc" && $sc != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`) VALUES ('$mis',1,'$category')";
                    $sc -= 1;
                }
                elseif($category == "st" && $st != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`) VALUES ('$mis',1,'$category')";
                    $st -= 1;
                }
                elseif($category == "obc" && $obc != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`) VALUES ('$mis',1,'$category')";
                    $obc -= 1;
                }
                else{
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`) VALUES ('$mis',0,'$category')";
                }
            }
            if($conn->query($insertIntoStatus)){

            }
            else{
                echo 'Error';
            }
            $count_open += 1;
        }
    }
    $tyBoysIt = "SELECT  mis,category,cgpa,year,branch,sbc,pwd from STUDENT where year = 4 and gender=1 and branch = 'it' and nri = 0 ORDER BY(cgpa) DESC";
    $result = $conn->query($tyBoysIt);
    if($result->num_rows > 0) {
        $count_open = 0;
        $sc = 3;
        $st = 2;
        $vj_ntb = 1;
        $ntc_ntd = 1;
        $obc = 5;
        while($row = $result->fetch_assoc()) {
            $mis = $row['mis'];
            $category = $row['category'];
            if($count_open < 12) {
                $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`) VALUES ('$mis',1,'$category')";
            }
            else{
                if($category == "vj/ntb" && $vj_ntb != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`) VALUES ('$mis',1,'$category')";
                    $vj_ntb -= 1;
                }
                elseif($category == "ntc/ntd" && $ntc_ntd != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`) VALUES ('$mis',1,'$category')";
                    $ntc_ntd -= 1;
                }
                elseif($category == "sc" && $sc != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`) VALUES ('$mis',1,'$category')";
                    $sc -= 1;
                }
                elseif($category == "st" && $st != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`) VALUES ('$mis',1,'$category')";
                    $st -= 1;
                }
                elseif($category == "obc" && $obc != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`) VALUES ('$mis',1,'$category')";
                    $obc -= 1;
                }
                else{
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`) VALUES ('$mis',0,'$category')";
                }
            }
            if($conn->query($insertIntoStatus)){

            }
            else{
                echo 'Error';
            }
            $count_open += 1;
        }
    }
    $tyBoysMeta = "SELECT  mis,category,cgpa,year,branch,sbc,pwd from STUDENT where year = 4 and gender=1 and branch = 'meta' and nri = 0 ORDER BY(cgpa) DESC";
    $result = $conn->query($tyBoysMeta);
    if($result->num_rows > 0) {
        $count_open = 0;
        $sc = 3;
        $st = 2;
        $vj_ntb = 1;
        $ntc_ntd = 1;
        $obc = 5;
        while($row = $result->fetch_assoc()) {
            $mis = $row['mis'];
            $category = $row['category'];
            if($count_open < 12) {
                $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`) VALUES ('$mis',1,'$category')";
            }
            else{
                if($category == "vj/ntb" && $vj_ntb != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`) VALUES ('$mis',1,'$category')";
                    $vj_ntb -= 1;
                }
                elseif($category == "ntc/ntd" && $ntc_ntd != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`) VALUES ('$mis',1,'$category')";
                    $ntc_ntd -= 1;
                }
                elseif($category == "sc" && $sc != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`) VALUES ('$mis',1,'$category')";
                    $sc -= 1;
                }
                elseif($category == "st" && $st != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`) VALUES ('$mis',1,'$category')";
                    $st -= 1;
                }
                elseif($category == "obc" && $obc != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`) VALUES ('$mis',1,'$category')";
                    $obc -= 1;
                }
                else{
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`) VALUES ('$mis',0,'$category')";
                }
            }
            if($conn->query($insertIntoStatus)){

            }
            else{
                echo 'Error';
            }
            $count_open += 1;
        }
    }
    $tyBoysProd = "SELECT  mis,category,cgpa,year,branch,sbc,pwd from STUDENT where year = 4 and gender=1 and branch = 'prod' and nri = 0 ORDER BY(cgpa) DESC";
    $result = $conn->query($tyBoysprod);
    if($result->num_rows > 0) {
        $count_open = 0;
        $sc = 3;
        $st = 2;
        $vj_ntb = 1;
        $ntc_ntd = 1;
        $obc = 5;
        while($row = $result->fetch_assoc()) {
            $mis = $row['mis'];
            $category = $row['category'];
            if($count_open < 12) {
                $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`) VALUES ('$mis',1,'$category')";
            }
            else{
                if($category == "vj/ntb" && $vj_ntb != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`) VALUES ('$mis',1,'$category')";
                    $vj_ntb -= 1;
                }
                elseif($category == "ntc/ntd" && $ntc_ntd != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`) VALUES ('$mis',1,'$category')";
                    $ntc_ntd -= 1;
                }
                elseif($category == "sc" && $sc != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`) VALUES ('$mis',1,'$category')";
                    $sc -= 1;
                }
                elseif($category == "st" && $st != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`) VALUES ('$mis',1,'$category')";
                    $st -= 1;
                }
                elseif($category == "obc" && $obc != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`) VALUES ('$mis',1,'$category')";
                    $obc -= 1;
                }
                else{
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`) VALUES ('$mis',0,'$category')";
                }
            }
            if($conn->query($insertIntoStatus)){

            }
            else{
                echo 'Error';
            }
            $count_open += 1;
        }
    }
    $tyBoysPlanning = "SELECT  mis,category,cgpa,year,branch,sbc,pwd from STUDENT where year = 4 and gender=1 and branch = 'planning' and nri = 0 ORDER BY(cgpa) DESC";
    $result = $conn->query($tyBoysPlanning);
    if($result->num_rows > 0) {
        $count_open = 0;
        $sc = 3;
        $st = 2;
        $vj_ntb = 1;
        $ntc_ntd = 1;
        $obc = 5;
        while($row = $result->fetch_assoc()) {
            $mis = $row['mis'];
            $category = $row['category'];
            if($count_open < 12) {
                $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`) VALUES ('$mis',1,'$category')";
            }
            else{
                if($category == "vj/ntb" && $vj_ntb != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`) VALUES ('$mis',1,'$category')";
                    $vj_ntb -= 1;
                }
                elseif($category == "ntc/ntd" && $ntc_ntd != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`) VALUES ('$mis',1,'$category')";
                    $ntc_ntd -= 1;
                }
                elseif($category == "sc" && $sc != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`) VALUES ('$mis',1,'$category')";
                    $sc -= 1;
                }
                elseif($category == "st" && $st != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`) VALUES ('$mis',1,'$category')";
                    $st -= 1;
                }
                elseif($category == "obc" && $obc != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`) VALUES ('$mis',1,'$category')";
                    $obc -= 1;
                }
                else{
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`) VALUES ('$mis',0,'$category')";
                }
            }
            if($conn->query($insertIntoStatus)){

            }
            else{
                echo 'Error';
            }
            $count_open += 1;
        }
    }

    $tyBoysInstru = "SELECT  mis,category,cgpa,year,branch,sbc,pwd from STUDENT where year = 4 and gender=1 and branch = 'instru' and nri = 0 ORDER BY(cgpa) DESC";
    $result = $conn->query($tyBoysInstru);
    if($result->num_rows > 0) {
        $count_open = 0;
        $sc = 1;
        $st = 1;
        $vj_ntb = 1;
        $ntc_ntd = 1;
        $obc = 2;
        while($row = $result->fetch_assoc()) {
            $mis = $row['mis'];
            $category = $row['category'];
            if($count_open < 5) {
                $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`) VALUES ('$mis',1,'$category')";
            }
            else{
                if($category == "vj/ntb" && $vj_ntb != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`) VALUES ('$mis',1,'$category')";
                    $vj_ntb -= 1;
                }
                elseif($category == "ntc/ntd" && $ntc_ntd != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`) VALUES ('$mis',1,'$category')";
                    $ntc_ntd -= 1;
                }
                elseif($category == "sc" && $sc != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`) VALUES ('$mis',1,'$category')";
                    $sc -= 1;
                }
                elseif($category == "st" && $st != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`) VALUES ('$mis',1,'$category')";
                    $st -= 1;
                }
                elseif($category == "obc" && $obc != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`) VALUES ('$mis',1,'$category')";
                    $obc -= 1;
                }
                else{
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`) VALUES ('$mis',0,'$category')";
                }
            }
            if($conn->query($insertIntoStatus)){

            }
            else{
                echo 'Error';
            }
            $count_open += 1;
        }
    }
?>