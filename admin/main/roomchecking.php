<?php
include('../../auth.php');
require_once('../../connection.php');
?>
<!DOCTYPE html>
<html lang="en">
  <head>
<!--  -->  	<!--  -->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>COEP HOSTEL | ADMISSION</title>
   <link href="../vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <!-- bootstrap-datetimepicker -->
    <link href="../vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- bootstrap-wysiwyg -->
    <link href="../vendors/google-code-prettify/bin/prettify.min.css" rel="stylesheet">
    <!-- Select2 -->  
    <link href="../vendors/select2/dist/css/select2.min.css" rel="stylesheet">
    <!-- Switchery -->
    <link href="../vendors/switchery/dist/switchery.min.css" rel="stylesheet">
    <!-- starrr -->
    <link href="../vendors/starrr/dist/starrr.css" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="../vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
    
    <!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css"> -->

<style>
@import "bourbon";

body {
  font-family: "Open Sans", "Segoe UI", Frutiger, "Frutiger Linotype", "Dejavu Sans", "Helvetica Neue", Arial, sans-serif;
  font-size: 14px;
  line-height: 1.5em;
  font-weight: 400;
}

p, span, a, ul, li, button {
  font-family: inherit;
  font-size: inherit;
  font-weight: inherit;
  line-height: inherit;
}

strong {
  font-weight: 600;
}

h1, h2, h3, h4, h5, h6 {
  font-family: "Open Sans", "Segoe UI", Frutiger, "Frutiger Linotype", "Dejavu Sans", "Helvetica Neue", Arial, sans-serif;
  line-height: 1.5em;
  font-weight: 300;
}

strong {
  font-weight: 400;
}

.tile {
  width: 100%;
  display: inline-block;
  box-sizing: border-box;
  background: #fff;
  padding: 20px;
  margin-bottom: 10px;
}
.tile .title {
  margin-top: 0px;
}
.tile.purple, .tile.blue, .tile.red, .tile.orange, .tile.green {
  color: #fff;
}
.tile.purple {
  background: #5133AB;
}
.tile.purple:hover {
  background: #3e2784;
}
.tile.red {
  background: #AC193D;
}
.tile.red:hover {
  background: #7f132d;
}
.tile.green {
  background: #00A600;
}
.tile.green:hover {
  background: #007300;
}
.tile.blue {
  background: #2672EC;
}
.tile.blue:hover {
  background: #125acd;
}
.tile.orange {
  background: #DC572E;
}
.tile.orange:hover {
  background: #b8431f;
}

</style>
</head>
<body>
<body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.php" class="site_title"><i class="fa fa-building-o " style="color : cyan;"></i> <span>COEP HOSTEL</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="images/user.png" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2><?php if(isset($_SESSION['name'])) echo $_SESSION['name']; else echo 'Admin'; ?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                  <li><a href="index.php"><i class="fa fa-home"></i> Home</a>
                  </li>
                  <li><a href="manager.php"><i class="fa fa-edit"></i> Manage </a>
                  </li>
                  <li><a href="genral.php"><i class="fa fa-cogs" ></i>Admin Actions</a>
                  </li>
                  <li><a href="studentlist.php"><i class="fa fa-folder"></i>Student List</a>
                  </li>
                  <li><a><i class="fa fa-building"></i>Allotment List<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                    <li><a href="seatdisplayaccepted.php"><i class="fa fa-check"></i>Alloted List</a></li>
                    <li><a href=seatdisplaywaiting.php><i class="fa fa-clock-o"></i>Waiting List</a></li>
                    </ul>
                  </li>
                  <li><a href="allotedroomsdisplay.php"><i class="fa fa-user"></i>Room List</a>
                  </li>
                  <li><a href="roomchecking.php"><i class="fa fa-check"></i>Room Check</a>
                  </li>
                </ul>
              </div>

            </div>
            
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="images/user.png" alt=""><?php if(isset($_SESSION['name'])) echo $_SESSION['name']; else echo 'Admin'; ?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    
                    <li><a href="logout.php"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>

                <li role="presentation" class="dropdown">
                  <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                    
                  </a>
                  
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->  

        <!-- page content -->
        <div class="right_col" role="main">
         <!--  <div class="container" align="center"> -->
              <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>CheckList<small>Select all those things which are in condition</small></h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                  <div class="">
                    <div class="form-group">
                      <label for="usr">Room Number:</label>
                      <input type="text" class="form-control" id="usr">
                    </div>
                    <div class="row">
                      <div class="form-group col-md-6 col-lg-6 col-sm-12">
                        <label for="usr">Name:</label>
                        <input type="text" class="form-control" id="usr">
                      </div>
                      <div class="col-md-1 col-lg-1"></div>
                      <div class="form-group col-md-5 col-lg-5 col-sm-12">
                        <label for="usr">Phone:</label>
                        <input type="text" class="form-control" id="usr">
                      </div>
                     </div> 
                     <br/>
                     <div class="row">
                        
                        <ul class="to_do">
                          <li>
                              <div class="form-check">
                                <input type="checkbox" class="form-check-input" id="materialUnchecked">
                                <label class="form-check-label" for="materialUnchecked">Light and AC supply check</label>
                              </div>
                          </li>
                          <li>
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" id="materialUnchecked1">
                                <label class="form-check-label" for="materialUnchecked1">Fan, remote and sensor check with it's working</label>
                            </div>
                          </li>
                          <li>
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" id="materialUnchecked2">
                                <label class="form-check-label" for="materialUnchecked2">Bed and mattress check</label>
                              </div>
                          </li>
                          <li>
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" id="materialUnchecked3">
                                <label class="form-check-label" for="materialUnchecked3">Wall color and patches check</label>
                            </div>
                          </li>
                          <li>
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" id="materialUnchecked4">
                                <label class="form-check-label" for="materialUnchecked4">Table and chair count and condition check</label>
                              </div>
                          </li>
                          <li>
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" id="materialUnchecked5">
                                <label class="form-check-label" for="materialUnchecked5">Window Glass and Door check</label>
                            </div> 
                          </li>
                        </ul>
                      </div>
                        <div class="row">
                          <div class="col-sm-12 col-md-12 col-lg-12">
                              <h3>Room Verification</h3>
                               <p style="align:center"></p><p> Enter the OTP you got after your room allocation</p>                             
                              <form method="post" id="veryfyotp" action="">
                                  <div class="row">                    
                                  <div class="form-group col-sm-8">
                                     <span style="color:red;"></span>                    
                                     <input type="text" class="form-control" name="otp" placeholder="Enter your OTP number" required="">
                                  </div>
                                  <button type="submit" class="btn btn-primary  pull-right col-sm-3">Verify</button>
                                  </div>
                              </form>
                            <br><br>
                          </div>
                      </div>
                      <div class="form-group">
                       <a href="roomchecking.php"><button type="submit" class="btn btn-success col-lg-12 col-sm-12 col-md-12">Submit</button></a>
                      </div>
                      
                  </div>
                </div>
              </div>
            </div>  
          
  			</div>
  
    

        <footer>
          <div class="pull-right">
            COEP HOSTEL ADMISSION PORTAL
          </div>
          <div class="clearfix"></div>
        </footer>
          </div>
        </div>
        <!-- /page content -->
        <!-- footer content -->
      





        <!-- /footer content -->

    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="../vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- jQuery Sparklines -->
    <script src="../vendors/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
    <!-- Flot -->
    <script src="../vendors/Flot/jquery.flot.js"></script>
    <script src="../vendors/Flot/jquery.flot.pie.js"></script>
    <script src="../vendors/Flot/jquery.flot.time.js"></script>
    <script src="../vendors/Flot/jquery.flot.stack.js"></script>
    <script src="../vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="../vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="../vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="../vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="../vendors/DateJS/build/date.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="../vendors/moment/min/moment.min.js"></script>
    <!-- <script src="../vendors/bootstrap-daterangepicker/daterangepicker.js"></script> -->
    <script src="../vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
    <!-- bootstrap-datetimepicker -->    
    <script src="../vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
    <!-- Ion -->
    
    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>
    <script type="text/javascript">
      $('#materialIndeterminate2').prop('indeterminate', true);
    </script>
    <script>
    $('#myDatepicker').datetimepicker();
    
    $('#myDatepicker2').datetimepicker({
        format: 'DD.MM.YYYY'
    });
    
    $('#myDatepicker3').datetimepicker({
        format: 'hh:mm A'
    });
    
    $('#myDatepicker4').datetimepicker({
        ignoreReadonly: true,
        allowInputToggle: true
    });

    $('#datetimepicker6').datetimepicker();
    
    $('#datetimepicker7').datetimepicker({
        useCurrent: false
    });
    
    $("#datetimepicker6").on("dp.change", function(e) {
        $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
    });
    
    $("#datetimepicker7").on("dp.change", function(e) {
        $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
    });
</script>
  </body>
</html>
