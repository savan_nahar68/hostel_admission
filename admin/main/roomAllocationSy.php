<?php
    error_reporting(0); 
    include('../../auth.php');
    require_once('../../connection.php');
    // echo "<script>alert('Room Allocation');</script>";
    $mis = $_SESSION['mis'];
    $sqlCivil = "SELECT mis from student where branch = 'civil' AND year = 3 AND mis IN (SELECT mis from status where accept = 1) ORDER BY(cgpa) DESC";
    $sqlComp = "SELECT mis from student where branch = 'comp' AND year = 3 AND mis IN (SELECT mis from status where accept = 1) ORDER BY(cgpa) DESC";
    $sqlElec = "SELECT mis from student where branch = 'electrical' AND year = 3 AND mis IN (SELECT mis from status where accept = 1) ORDER BY(cgpa) DESC";
    $sqlEntc = "SELECT mis from student where branch = 'entc' AND year = 3 AND mis IN (SELECT mis from status where accept = 1) ORDER BY(cgpa) DESC";
    $sqlInstru = "SELECT mis from student where branch = 'instru' AND year = 3 AND mis IN (SELECT mis from status where accept = 1) ORDER BY(cgpa) DESC";
    $sqlIt = "SELECT mis from student where branch = 'it' AND year = 3 AND mis IN (SELECT mis from status where accept = 1) ORDER BY(cgpa) DESC";
    $sqlMech = "SELECT mis from student where branch = 'mech' AND year = 3 AND mis IN (SELECT mis from status where accept = 1) ORDER BY(cgpa) DESC";
    $sqlMeta = "SELECT mis from student where branch = 'meta' AND year = 3 AND mis IN (SELECT mis from status where accept = 1) ORDER BY(cgpa) DESC";
    $sqlPlanning = "SELECT mis from student where branch = 'planning' AND year = 3 AND mis IN (SELECT mis from status where accept = 1) ORDER BY(cgpa) DESC";
    $sqlProd = "SELECT mis from student where branch = 'prod' AND year = 3 AND mis IN (SELECT mis from status where accept = 1) ORDER BY(cgpa) DESC";
    
    $allocatedRoom = array();
    $allocatedMIS = array();
    $wait = array();

    $civil= $conn->query($sqlCivil);
    $comp = $conn->query($sqlComp);
    $elec = $conn->query($sqlElec);
    $entc = $conn->query($sqlEntc);
    $instru = $conn->query($sqlInstru);
    $it = $conn->query($sqlIt);
    $mech = $conn->query($sqlMech);
    $meta = $conn->query($sqlMeta);
    $plannng = $conn->query($sqlPlanning);
    $prod = $conn->query($sqlProd);

    $roomCountSql = "SELECT count(*) from roomsstatus where 1";
    // echo $roomCountSql;
    $result = $conn->query($roomCountSql);
    $data = $result->fetch_assoc();
    $roomCount = $data['count(*)'];
    // echo $roomCount;
    $students = 1;
    
    function allocateRoom($mis, $conn) {
        global $allocatedMIS, $allocatedRoom,$wait,$roomCount;
        $roomPrefSql = "SELECT * FROM `priority` WHERE mis = '$mis'";
        // echo $roomPrefSql;
        $result12 = $conn->query($roomPrefSql);
        // echo $result12->num_rows;
        // echo "done";
        if($result12->num_rows){
            while($row = $result12->fetch_assoc()) {
                // echo $row['priority'];
                $val =  explode(',',$row['priority']);
                
                $i = 0;
                // echo $val[$i];
                while(in_array($val[$i],$allocatedRoom)){
                    $i++;
                }
                // echo $i;
                if($i < count($val)) {
                        $sqlRoomMates = "SELECT * FROM roommates where main = '$mis' or roommate1 = '$mis' or roommate2 = '$mis' or roommate3 = '$mis'";
                        // echo $sqlRoomMates;
                        $roommates = $conn->query($sqlRoomMates);
                        $data = $roommates->fetch_assoc();
                        // if(isset($data['main']) && isset($data['roommate1']) && isset($data['roommate2']) && isset($data['roommate3']) )
                        array_push($allocatedMIS,$data['main']);
                        array_push($allocatedMIS,$data['roommate1']);
                        array_push($allocatedMIS,$data['roommate2']);   
                        array_push($allocatedMIS,$data['roommate3']);
                        $room = $val[$i];
                        array_push($allocatedRoom,$rooms);
                        $sqlRoom = "UPDATE `roomsstatus` SET `status`='1',`mis`='$mis' WHERE roomno = '$room'";
                        // echo $sqlRoom;
                        $conn->query($sqlRoom);
                        $sqlRoom = "UPDATE `roommates` SET `room_number`='$room' WHERE main = '$mis' or roommate1 = '$mis' or roommate2 = '$mis' or roommate3 = '$mis'";
                    $conn->query($sqlRoom);
                        $roomCount--;
                }
                else {
                    array_push($wait,$mis);
                    // print_r($wait);   
                }
            }
        }
        else {
            array_push($wait,$mis);
            // print_r($wait);
        }
    }

    while($roomCount && $students) {
        // echo "Innnn";
        // echo $roomCount;
        $row1 = $civil->fetch_assoc();
        $row2 = $comp->fetch_assoc();
        $row3 = $elec->fetch_assoc();
        $row4 = $entc->fetch_assoc();
        $row5 = $instru->fetch_assoc();
        $row6 = $it->fetch_assoc();
        $row7 = $mech->fetch_assoc();
        $row8 = $meta->fetch_assoc();
        $row9 = $plannng->fetch_assoc();
        $row10 = $prod->fetch_assoc();
        
        while(isset($row1) && in_array($row1['mis'],$allocatedMIS)) {
            $row1 = $civil->fetch_assoc();
        }
        while(isset($row2) && in_array($row2['mis'],$allocatedMIS)) {
            $row2 = $comp->fetch_assoc();
        }while(isset($row3) && in_array($row3['mis'],$allocatedMIS)) {
            $row3 = $elec->fetch_assoc();
        }while(isset($row4) && in_array($row4['mis'],$allocatedMIS)) {
            $row4 = $entc->fetch_assoc();
        }while(isset($row5) && in_array($row5['mis'],$allocatedMIS)) {
            $row5 = $instru->fetch_assoc();
        }while(isset($row6) && in_array($row6['mis'],$allocatedMIS)) {
            $row6 = $it->fetch_assoc();
        }while(isset($row7) && in_array($row7['mis'],$allocatedMIS)) {
            $row7 = $mech->fetch_assoc();
        }while(isset($row8) && in_array($row8['mis'],$allocatedMIS)) {
            $row8 = $meta->fetch_assoc();
        }while(isset($row9) && in_array($row9['mis'],$allocatedMIS)) {
            $row9 = $planning->fetch_assoc();
        }while(isset($row10) && in_array($row10['mis'],$allocatedMIS)) {
            $row10 = $prod->fetch_assoc();
        }
        if($row1 || $row2 || $row3 || $row4 || $row5 || $row6 || $row7 || $row8 || $row9 || $row10) {
            if($row1)allocateRoom($row1['mis'],$conn);
            if($row2)allocateRoom($row2['mis'],$conn);
            if($row3)allocateRoom($row3['mis'],$conn);
            if($row4)allocateRoom($row4['mis'],$conn);
            if($row5)allocateRoom($row5['mis'],$conn);
            if($row6)allocateRoom($row6['mis'],$conn);
            if($row7)allocateRoom($row7['mis'],$conn);
            if($row8)allocateRoom($row8['mis'],$conn);
            if($row9)allocateRoom($row9['mis'],$conn);
            if($row10)allocateRoom($row10['mis'],$conn);
        }
        else {
            $students = 0;
        }
    }
    // echo $roomCount;
    $i = 0;
    $sqlVacantRooms = "SELECT roomno from roomsstatus where status = '0'";
    // echo $sqlVacantRooms;
    $vacantRooms = $conn->query($sqlVacantRooms);
    $count = count($wait);
    // echo $count;
    while($count && ($room= $vacantRooms->fetch_assoc())) {
        // echo "parat";
        $mis = $wait[$i];
        // $sqlRoomMates = "SELECT * FROM roommates where main = '$mis' or roommate1 = '$mis' or roommate2 = '$mis' or roommate3 = '$mis'";
        // $roommates = $conn->query($sqlRoomMates);
        // $data = $roommates->fetch_assoc();
        if($wait[$i])
            array_push($allocatedMIS,$wait[$i++]);
        if($wait[$i])
            array_push($allocatedMIS,$wait[$i++]);
        if($wait[$i])
            array_push($allocatedMIS,$wait[$i++]);
        if($wait[$i])
            array_push($allocatedMIS,$wait[$i++]);
        
        $room = $room['roomno'];
        array_push($allocatedRoom,$rooms);
        $sqlRoom = "UPDATE `roomsstatus` SET `status`='1',`mis`='$mis' WHERE roomno = '$room'";
        $conn->query($sqlRoom);
        $sqlRoom = "INSERT INTO `roommates`(`room_number`) VALUES('$room') WHERE main = '$mis' or roommate1 = '$mis' or roommate2 = '$mis' or roommate3 = '$mis'";
        $conn->query($sqlRoom);
        $roomCount--;
        $i++;
    }
    $sqltab = "Update tabs set openroomlist = 1 where id = 1";
    $result1 = $conn->query($sqltab);
    $sql11 = "update student set accept = 5 where mis = '$mis'";
    $result = $conn->query($sql11);
    $sql = "INSERT INTO `notices`(`content`, `author`) VALUES ('You have been allocated room number E305','Rector COEP')" ;
	 $result = $conn->query($sql);
	if($result) {
		echo "Success..!";
	}
	else echo "Error while saving";
?>