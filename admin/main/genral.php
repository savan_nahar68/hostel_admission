<?php
  include('../../auth.php');
  require_once('../../connection.php');
  if(isset($_POST['inputBlackList'])) {
      $blockmis = $_POST['inputBlackList'];
      $sql ="Update student set block = '1' where mis ='$blockmis'";
      $result = $conn->query($sql);
  }
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>COEP | HOSTEL ADMISSION </title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    
    <!-- Custom styling plus plugins -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.php" class="site_title"><i class="fa fa-building-o style="color : cyan;""></i> <span>COEP HOSTEL</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="images/user.png" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2><?php if(isset($_SESSION['name'])) echo $_SESSION['name']; else echo 'Admin'; ?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                  <li><a href="index.php"><i class="fa fa-home"></i> Home</a>
                  </li>
                  <li><a href="manager.php"><i class="fa fa-edit"></i> Manage </a>
                  </li>
                  <li><a href="genral.php"><i class="fa fa-cogs" ></i>Admin Actions</a>
                  </li>
                  <li><a href="studentlist.php"><i class="fa fa-folder"></i>Student List</a>
                  </li>
                  <li><a><i class="fa fa-building"></i>Allotment List<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                    <li><a href="seatdisplayaccepted.php"><i class="fa fa-check"></i>Alloted List</a></li>
                    <li><a href=seatdisplaywaiting.php><i class="fa fa-clock-o"></i>Waiting List</a></li>
                    </ul>
                  </li>
                  <li><a href="allotedroomsdisplay.php"><i class="fa fa-user"></i>Room List</a>
                  </li>
                  <li><a href="roomchecking.php"><i class="fa fa-check"></i>Room Check</a>
                  </li>
                </ul>
              </div>

            </div>
            
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="images/user.png" alt=""><?php if(isset($_SESSION['name'])) echo $_SESSION['name']; else echo 'Admin'; ?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    
                    <li><a href="logout.php"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>

                <li role="presentation" class="dropdown">
                  <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                    
                  </a>
                  
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Extras</h3>
              </div>

              
            </div>
            
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Mass mail</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <form action="#" method="POST">
                       <label class="btn" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                              <input type="radio" name="users_type" value="male"> &nbsp; All Users &nbsp;
                       </label>
                        <label class="btn" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                              <input type="radio" name="users_type" value="male"> &nbsp; Subscribed Users &nbsp;
                       </label>
                       <label class="btn" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                              <input type="radio" name="users_type" value="male"> &nbsp; Non-Subscribed Users &nbsp;
                      </label>
                       
                      <!-- <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                        <input type="text" class="form-control" id="inputSuccess3" placeholder="MIS ID">
                        <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                      </div>    
 -->
                      <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                        <input type="text" class="form-control" id="inputSuccess3" placeholder="Subject">
                        <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                      </div>
                      <!-- text area -->
              <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Message:</h2>
                  
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <div id="alerts"></div>
                  <div class="btn-toolbar editor" data-role="editor-toolbar" data-target="#editor-one">
                    <div class="btn-group">
                      <a class="btn dropdown-toggle" data-toggle="dropdown" title="Font" aria-expanded="false"><i class="fa fa-font"></i><b class="caret"></b></a>
                      <ul class="dropdown-menu">
                      </ul>
                    </div>

                    <div class="btn-group">
                      <a class="btn dropdown-toggle" data-toggle="dropdown" title="Font Size" aria-expanded="false"><i class="fa fa-text-height"></i>&nbsp;<b class="caret"></b></a>
                      <ul class="dropdown-menu">
                        <li>
                          <a data-edit="fontSize 5" class="">
                            <p style="font-size:17px">Huge</p>
                          </a>
                        </li>
                        <li>
                          <a data-edit="fontSize 3" class="">
                            <p style="font-size:14px">Normal</p>
                          </a>
                        </li>
                        <li>
                          <a data-edit="fontSize 1">
                            <p style="font-size:11px">Small</p>
                          </a>
                        </li>
                      </ul>
                    </div>

                    <div class="btn-group">
                      <a class="btn" data-edit="bold" title="Bold (Ctrl/Cmd+B)"><i class="fa fa-bold"></i></a>
                      <a class="btn" data-edit="italic" title="Italic (Ctrl/Cmd+I)"><i class="fa fa-italic"></i></a>
                      <a class="btn" data-edit="strikethrough" title="Strikethrough"><i class="fa fa-strikethrough"></i></a>
                      <a class="btn" data-edit="underline" title="Underline (Ctrl/Cmd+U)"><i class="fa fa-underline"></i></a>
                    </div>

                    <div class="btn-group">
                      <a class="btn" data-edit="insertunorderedlist" title="Bullet list"><i class="fa fa-list-ul"></i></a>
                      <a class="btn" data-edit="insertorderedlist" title="Number list"><i class="fa fa-list-ol"></i></a>
                      <a class="btn" data-edit="outdent" title="Reduce indent (Shift+Tab)"><i class="fa fa-dedent"></i></a>
                      <a class="btn" data-edit="indent" title="Indent (Tab)"><i class="fa fa-indent"></i></a>
                    </div>

                    <div class="btn-group">
                      <a class="btn" data-edit="justifyleft" title="Align Left (Ctrl/Cmd+L)"><i class="fa fa-align-left"></i></a>
                      <a class="btn" data-edit="justifycenter" title="Center (Ctrl/Cmd+E)"><i class="fa fa-align-center"></i></a>
                      <a class="btn" data-edit="justifyright" title="Align Right (Ctrl/Cmd+R)"><i class="fa fa-align-right"></i></a>
                      <a class="btn" data-edit="justifyfull" title="Justify (Ctrl/Cmd+J)"><i class="fa fa-align-justify"></i></a>
                    </div>

                    <div class="btn-group">
                      <a class="btn dropdown-toggle" data-toggle="dropdown" title="Hyperlink"><i class="fa fa-link"></i></a>
                      <div class="dropdown-menu input-append">
                        <input class="span2" placeholder="URL" data-edit="createLink" type="text">
                        <button class="btn" type="button">Add</button>
                      </div>
                      <a class="btn" data-edit="unlink" title="Remove Hyperlink"><i class="fa fa-cut"></i></a>
                    </div>

                    <div class="btn-group">
                      <a class="btn" title="Insert picture (or just drag &amp; drop)" id="pictureBtn"><i class="fa fa-picture-o"></i></a>
                      <input data-role="magic-overlay" data-target="#pictureBtn" data-edit="insertImage" type="file">
                    </div>

                    <div class="btn-group">
                      <a class="btn" data-edit="undo" title="Undo (Ctrl/Cmd+Z)"><i class="fa fa-undo"></i></a>
                      <a class="btn" data-edit="redo" title="Redo (Ctrl/Cmd+Y)"><i class="fa fa-repeat"></i></a>
                    </div>
                  </div>

                  <div id="editor-one" class="editor-wrapper placeholderText" contenteditable="true"><div align="right"><blockquote><i><font size="3"></font></i></blockquote></div></div>

                  <textarea name="descr" id="descr" style="display:none;"></textarea>
                  
                  <br>

                  
                </div>
                 <button class="btn btn-primary" type="submit">Send</button>
              </div>
            </div>  



                    </form>  
        <!-- /footer content -->
      </div>
    </div>
  </div>
</div>

          <div class="row">
            <div class="col-md-4 col-sm-12 col-lg-4">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Disciplinary Action</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                      <form action="#" method="POST">
                        <input type="text" class="form-control" id="inputDisciplinary" placeholder="MIS">
                        <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span><br>
                        <button class="btn btn-primary" type="submit">Send</button>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-4 col-lg-4 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Add to black List</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <form action="#" method="POST">
                        <input type="text" name='inputBlackList' class="form-control" id="inputBlackList" placeholder="MIS">
                        <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                        <br>
                        <button class="btn btn-primary" type="submit">Send</button>
                    </form>
                  </div>
                </div>
              </div>
              <div class="col-md-4 col-sm-12 col-lg-4">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Reserve rooms</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                      <form action="#" method="POST">
                        <input type="text" class="form-control" id="inputDisciplinary" placeholder="Enter Room No.">
                        <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span><br>
                        <button class="btn btn-primary" type="submit">Reserve</button>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
          </div>
</div>
</div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            COEP HOSTEL ADMISSION PORTAL
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>
  </body>
</html>