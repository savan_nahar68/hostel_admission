<!DOCTYPE html>
<?php
  include('../../auth.php');
  require_once('../../connection.php');
?>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>COEP | HOSTEL ADMISSION</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- bootstrap-wysiwyg -->
    <link href="../vendors/google-code-prettify/bin/prettify.min.css" rel="stylesheet">
    <!-- Select2 -->  
    <link href="../vendors/select2/dist/css/select2.min.css" rel="stylesheet">
    <!-- Switchery -->
    <link href="../vendors/switchery/dist/switchery.min.css" rel="stylesheet">
    <!-- starrr -->
    <link href="../vendors/starrr/dist/starrr.css" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="../vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
    <style>
      * {
          box-sizing: border-box;
      }

      /* Create two equal columns that floats next to each other */
      .column {
          float: left;
          width: 50%;
          padding: 10px;
      }

      /* Clear floats after the columns */
      .row:after {
          content: "";
          display: table;
          clear: both;
      }
     img {
          transition:transform 0.25s ease;
      }

      img:hover {
          -webkit-transform:scale(3.5);
          transform:scale(3.5);
      }
      </style>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.php" class="site_title"><i class="fa fa-building-o " style="color : cyan;"></i> <span>COEP HOSTEL</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="images/user.png" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2><?php if(isset($_SESSION['name'])) echo $_SESSION['name']; else echo 'Admin'; ?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                  <li><a href="index.php"><i class="fa fa-home"></i> Home</a>
                  </li>
                  <li><a href="manager.php"><i class="fa fa-edit"></i> Manage </a>
                  </li>
                  <li><a href="genral.php"><i class="fa fa-cogs" ></i>Admin Actions</a>
                  </li>
                  <li><a href="studentlist.php"><i class="fa fa-folder"></i>Student List</a>
                  </li>
                  <li><a><i class="fa fa-building"></i>Allotment List<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                    <li><a href="seatdisplayaccepted.php"><i class="fa fa-check"></i>Alloted List</a></li>
                    <li><a href=seatdisplaywaiting.php><i class="fa fa-clock-o"></i>Waiting List</a></li>
                    </ul>
                  </li>
                  <li><a href="allotedroomsdisplay.php"><i class="fa fa-user"></i>Room List</a>
                  </li>
                  <li><a href="roomchecking.php"><i class="fa fa-check"></i>Room Check</a>
                  </li>
                </ul>
              </div>

            </div>
            
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="images/user.png" alt=""><?php if(isset($_SESSION['name'])) echo $_SESSION['name']; else echo 'Admin'; ?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    
                    <li><a href="logout.php"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>

                <li role="presentation" class="dropdown">
                  <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                    
                  </a>
                  
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- <div class=""> -->
       <div class="right_col" role="main" style="min-height : 800px">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>All Student List</h3>
              </div>
            </div>

          </div><br><br>
         <div class="x_content">

                     <div class="x_panel">
                  <div class="x_content">

                    <div class="btn-group col-md-2 col-lg-2 col-sm-12">
                    <button data-toggle="dropdown" id="btn-year" class="btn btn-primary dropdown-toggle btn-lg" type="button" aria-expanded="false">Year <span class="caret"></span>
                    </button>
                    <ul role="menu" class="dropdown-menu" id='year'>
                      <li><a onclick="enable1('First Year');" value = 'fy'>First Year</a>
                      </li>
                      <li><a onclick="enable1('Second Year');" value = 'sy'>Second Year</a>
                      </li>
                      <li><a onclick="enable1('Third Year');" value = 'ty'>Third Year</a>
                      </li>
                      <li><a onclick="enable1('Fourth Year');" value = 'bt'>Fourth Year</a>
                      </li>
                    </ul>
                    </div>
                    
                    <div class="btn-group col-md-2 col-lg-2 col-sm-12" id = 'branch'>
                    <button data-toggle="dropdown" id="btn-branch" class="btn btn-primary dropdown-toggle btn-lg" type="button" aria-expanded="false" disabled ">Branch <span class="caret"></span>
                    </button>
                    <ul role="menu" class="dropdown-menu" id="branch" onselect="enable12();">
                      <li><a onclick="enable2('Computer');" value = 'comp'>Computer</a>
                      </li>
                      <li><a onclick="enable2('IT');" value = 'it'>IT</a>
                      </li>
                      <li><a onclick="enable2('Mechanical');" value = 'mech'>Mechanical</a>
                      </li>
                      <li><a onclick="enable2('Metullurgy');" value = 'meta'>Metullurgy</a>
                      </li>
                      <li><a onclick="enable2('Instrumentation');" value = 'instru'>Instrumentation</a>
                      </li>
                      <li><a onclick="enable2('E&TC');" value = 'entc'>E&TC</a>
                      </li>
                      <li><a onclick="enable2('Civil');" value = 'civil'>Civil</a>
                      </li>
                      <li><a onclick="enable2('Electrical');" value = 'electrical'>Electrical</a>
                      </li>
                      <li><a onclick="enable2('Production');" value = 'electrical'>Electrical</a>
                      </li>
                      <li><a onclick="enable2('Planning');" value = 'electrical'>Electrical</a>
                      </li>
                    </ul>
                    </div>
                    
                    <div class="btn-group col-md-2 col-lg-2 col-sm-12" id='category'>
                    <button data-toggle="dropdown" id="btn-caste" class="btn btn-primary dropdown-toggle btn-lg" type="button" disabled="true">Category <span class="caret"></span>
                    </button>
                    <ul role="menu" class="dropdown-menu" id="caste" onselect="showCaste();">
                      <li><a onclick="showCaste('Open');" value = 'open'>Open</a>
                      </li>
                      <li><a onclick="showCaste('SC');" value = 'sc'>SC</a>
                      </li>
                      <li><a onclick="showCaste('ST');" value = 'st'>ST</a>
                      </li>
                      <li><a onclick="showCaste('VJ/NTB');" value = 'vj/ntb'>VJ/NTB</a>
                      </li>
                      <li><a onclick="showCaste('NT(C)/NT(D)');" value = 'ntc/ntd'>NTC/NTD</a>
                      </li>
                      <li><a onclick="showCaste('OBC');" value = 'obc'>OBC</a>
                      </li>                      
                    </ul>
                    <!-- <input type="text" name="txtprice" id="txtprice" onClick="javascript:$('select.foo option:selected').val();"> -->
                    </div>
                       <div class="btn-group col-md-4 col-lg-4 col-sm-12" id='category'>
                    <button data-toggle="dropdown" id="btn-gender" class="btn btn-primary dropdown-toggle btn-lg" type="button" disabled="true">Gender <span class="caret"></span>
                    </button>
                    <ul role="menu" class="dropdown-menu" id="gender" onselect="showgender();">
                      <li><a onclick="showgender('Male');" value = 'Male'>Male</a>
                      </li>
                      <li><a onclick="showgender('Female');" value = 'Female'>Female</a>
                      </li>
                                            
                    </ul>
                    <!-- <input type="text" name="txtprice" id="txtprice" onClick="javascript:$('select.foo option:selected').val();"> -->
                    </div>


                    <div class="col-md-2 col-lg-2 col-sm-12" id='show'>
                    <button id="btn-show" class="btn btn-success btn-lg" type="button" onclick="showme();">Show
                    </button>
                     </div> 
                     
                  </div>
                  <div style="display: none;" id="datatableshow">
                    <table id="example" class="table table-striped table-bordered" style="width:100%">
                          <thead>
                              <tr>
                                  <th>MIS</th>
                                  <th>Name</th>
                                  <th>Surname</th>
                                  <th>CGPA</th>
                                  <th>Document</th>
                                  <th>Reject</th>
                              </tr>
                          </thead>
                              <tbody>
                                <?php 
                                    $mis = '111603012';
                                    $name = 'Default N';
                                    $branch = 'comp';
                                    $year = 3;
                                    $cgpa = 6.7;
                                    $category = 'open';
                                    $sql2 = "select mis,fname,lname,branch,year,category,cgpa from student where year = 3 and branch = 'comp' order by cgpa desc";
                                    //echo $sql2;
                                    $result2 = $conn->query($sql2);
                                    if ($result2->num_rows > 0) {
                                        while($row2 = $result2->fetch_assoc()) {
                                            $name =$row2['fname'];
                                            $lastname = $row2['lname'];
                                            $branch = $row2['branch'];
                                            $year = $row2['year'];
                                            $cgpa = $row2['cgpa'];
                                            $category = $row2['category']; 
                                        
                                ?>
                                <tr>
                                    <td><?php echo $mis; ?></td>
                                    <td><?php echo $name; ?></td>
                                    <td><?php echo $lastname; ?></td>
                                    <td><?php echo $cgpa; ?></td>
                                    <td style="text-align: center;"><button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModal">Documents</button></td>
                                    <td style="text-align: center;"><button class = "btn btn-warning btn-sm" type="button" data-toggle="button">Reject</button></td>
                                    
                                </tr>
                                <?php
                                        }
                                    }
                                    else{
                                        $mis = '111603012';
                                        $name = 'Default N';
                                        $branch = 'comp';
                                        $year = 3;
                                        $cgpa = 6.7;
                                        $category = 'open';
                                    }
                                ?>
                          </tbody>
                          <tfoot>
                              <tr>
                                  <th>Name</th>
                                  <th>Position</th>
                                  <th>Office</th>
                                  <th>Age</th>
                                  <th>Start date</th>
                                  <th>Salary</th>
                              </tr>
                          </tfoot>
                  </table>
              </div>
              <div class = "col-md-12 col-lg-12 col-sm-12" id="submitbutton" style="display:none;">
                  <form action ="acceptStudentList.php" method ="POST" >
                     <input type="hidden" name="year" id="year" value = "null"/>
                     <input type="hidden" name="branch" id="branch" value = "null"/>
                     <input type="hidden" name="caste" id="caste" value = "null"/>
                     <input type="hidden" name="gender" id="gender" value = "null"/>  
                  <input class = "col-md-12 col-lg-12 col-sm-12 btn btn-success btn-lg" type="submit" name ='submit' value = "Accept">
                   </form>            
              </div>
           </div>
        
                

<!-- Modal -->
        <div class="modal fade" id="myModal" role="dialog">
          <div class="modal-dialog modal-md">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Documents</h4>
              </div>
              <div class="modal-body">
                <div class="row">
                  <div class="column" style="background-color:#aaa;">
                    <center><img src="images/img.jpg"></center>
                  </div>
                  <div class="column" style="background-color:#bbb;">
                    <center><img src="images/img.jpg"></center>
                  </div>
                </div>

                <div class="row">
                  <div class="column" style="background-color:#ccc;">
                    <center><img src="images/img.jpg"></center>
                  </div>
                  <div class="column" style="background-color:#ddd;">
                    <center><img src="images/img.jpg"></center>
                  </div>
                </div>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>

          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
       
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            COEP HOSTEL ADMISSION PORTAL
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>
   <script type="text/javascript">
          
          var dropdown = " <span class='caret'></span>";
          function enable1(year) {
            document.getElementById('btn-branch').disabled = false;
            document.getElementById('btn-year').innerHTML = year + dropdown;
            $('input[name=year]').val(year);
          }
          function enable2(branch) {
            document.getElementById('btn-caste').disabled = false;
            document.getElementById('btn-branch').innerHTML = branch + dropdown;
            $('input[name=branch]').val(branch);
          }
          function showCaste(caste) {
            document.getElementById('btn-gender').disabled = false;
            document.getElementById('btn-caste').innerHTML = caste + dropdown;
            $('input[name=caste]').val(caste);
          }
          function showme(){
            document.getElementById('datatableshow').style = 'display:block;';
            document.getElementById('submitbutton').style = 'display:block;';
          }
          function showgender(gender) {
             document.getElementById('btn-gender').innerHTML = gender + dropdown;
            $('input[name=gender]').val(gender);
          }
        </script>
          <script>
          windows.onload('gridView()');
        // Get the elements with class="column"
        var elements = document.getElementsByClassName("column");

        // Declare a loop variable
        var i;

          for (i = 0; i < elements.length; i++) {
            elements[i].style.width = "50%";
          }

        /* Optional: Add active class to the current button (highlight it) */
        var container = document.getElementById("btnContainer");
        var btns = container.getElementsByClassName("btn");
        for (var i = 0; i < btns.length; i++) {
          btns[i].addEventListener("click", function(){
            var current = document.getElementsByClassName("active");
            current[0].className = current[0].className.replace(" active", "");
            this.className += " active";
          });
        }
        </script>
      
    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="../vendors/iCheck/icheck.min.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="../vendors/moment/min/moment.min.js"></script>
    <script src="../vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
    <!-- bootstrap-wysiwyg -->
    <script src="../vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js"></script>
    <script src="../vendors/jquery.hotkeys/jquery.hotkeys.js"></script>
    <script src="../vendors/google-code-prettify/src/prettify.js"></script>
    <!-- jQuery Tags Input -->
    <script src="../vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>
    <!-- Switchery -->
    <script src="../vendors/switchery/dist/switchery.min.js"></script>
    <!-- Select2 -->
    <script src="../vendors/select2/dist/js/select2.full.min.js"></script>
    <!-- Parsley -->
    <script src="../vendors/parsleyjs/dist/parsley.min.js"></script>
    <!-- Autosize -->
    <script src="../vendors/autosize/dist/autosize.min.js"></script>
    <!-- jQuery autocomplete -->
    <script src="../vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js"></script>
    <!-- starrr -->
    <script src="../vendors/starrr/dist/starrr.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>
  
  </body>
</html>
