<?php
include('../../auth.php');
require('../../connection.php'); 
?>
<!DOCTYPE html>
<html lang="en">
<style>
  .list-group{
    max-height: 300px;
    margin-bottom: 10px;
    overflow:scroll;
    -webkit-overflow-scrolling: touch;
    float: left;
    border: 0 !important;
    margin: 0px;
    padding: 10px 0px;
}
</style>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>COEP HOSTEL |ADMISSION</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
	
    <!-- bootstrap-progressbar -->
    <link href="../vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="../vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="../vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
    <script src="../../amcharts/amcharts/amcharts.js"></script>
  <script src="../../amcharts/amcharts/serial.js"></script>
  <style>
#chartdiv {
  width: 100%;
  height: 300px;
}										
</style>

<!-- Resources -->
<!-- <script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="https://www.amcharts.com/lib/3/serial.js"></script> -->
<script src="../../amcharts/amcharts/export/export.min.js"></script>
<link rel="stylesheet" href="../../amcharts/amcharts/plugins/export/export.css" type="text/css" media="all" />
<script src="../../amcharts/amcharts/themes/light.js"></script>

<!-- Chart code -->

<script>
var chart = AmCharts.makeChart("chartdiv", {
    "theme": "light",
    "type": "serial",
	"startDuration": 0.5,
    "dataProvider": [{
        "country": "A",
        "visits": 110,
        "color": "#FF0F00"
    }, {
        "country": "B",
        "visits": 160,
        "color": "#FF6600"
    }, {
        "country": "C",
        "visits": 360,
        "color": "#FF9E01"
    }, {
        "country": "D",
        "visits": 420,
        "color": "#FCD202"
    }, {
        "country": "E",
        "visits": 380,
        "color": "#F8FF01"
    }, {
        "country": "F",
        "visits": 260,
        "color": "#B0DE09"
    }, {
        "country": "G",
        "visits": 380,
        "color": "#04D215"
    }, {
        "country": "H",
        "visits": 340,
        "color": "#0D8ECF"
    }, {
        "country": "I",
        "visits": 390,
        "color": "#0D52D1"
    }],
    "valueAxes": [{
        "position": "left",
        "title": "Capacity of Students"
    }],
    "graphs": [{
        "balloonText": "[[category]]: <b>[[value]]</b>",
        "fillColorsField": "color",
        "fillAlphas": 1,
        "lineAlpha": 0.1,
        "type": "column",
        "valueField": "visits",
        "fixedColumnWidth" : 25
    }],
    "depth3D": 20,
	"angle": 30,
    "chartCursor": {
        "categoryBalloonEnabled": false,
        "cursorAlpha": 0,
        "zoomable": false
    },
    "categoryField": "country",
    "categoryAxis": {
        "gridPosition": "start",
        "labelRotation": 90
    },
    "export": {
    	"enabled": true
     }

}); 
function resize() {
document.getElementById('chartdiv').style.width = '300px';
document.getElementById('chartdiv').style.height = '300px';
}
</script>

  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.php" class="site_title"><i class="fa fa-building-o " style="color : cyan;"></i> <span>COEP HOSTEL</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="images/user.png" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2><?php if(isset($_SESSION['name'])) echo $_SESSION['name']; else echo 'Admin'; ?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                  <li><a href="index.php"><i class="fa fa-home"></i> Home</a>
                  </li>
                  <li><a href="manager.php"><i class="fa fa-edit"></i> Manage </a>
                  </li>
                  <li><a href="genral.php"><i class="fa fa-cogs" ></i>Admin Actions</a>
                  </li>
                  <li><a href="studentlist.php"><i class="fa fa-folder"></i>Student List</a>
                  </li>
                  <li><a><i class="fa fa-building"></i>Allotment List<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                    <li><a href="seatdisplayaccepted.php"><i class="fa fa-check"></i>Alloted List</a></li>
                    <li><a href=seatdisplaywaiting.php><i class="fa fa-clock-o"></i>Waiting List</a></li>
                    </ul>
                  </li>
                  <li><a href="allotedroomsdisplay.php"><i class="fa fa-user"></i>Room List</a>
                  </li>
                  <li><a href="roomchecking.php"><i class="fa fa-check"></i>Room Check</a>
                  </li>
                </ul>
              </div>

            </div>
            
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="images/user.png" alt=""><?php if(isset($_SESSION['name'])) echo $_SESSION['name']; else echo 'Admin'; ?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    
                    <li><a href="logout.php"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>

                <li role="presentation" class="dropdown">
                  <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                    
                  </a>
                  
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->
        <?php
          //query for all users
          $sql = "SELECT count(mis) as count FROM student";
          $result = $conn->query($sql);
          if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
              $count = $row['count'];
            }
          }else {
            $count = 0;
          }
          //query for male
          $sql = "SELECT count(gender) as male FROM student where gender = 0";
          $sql1 = "SELECT count(nri) as nri FROM student where nri = 1";
          $result = $conn->query($sql);
          
          if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
              $male = $row['male'];
            }
          }else {
            $male = 0;
          }
          $female = $count - $male;
          //for NRI count
          $result = $conn->query($sql1);
          
          if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
              $nri = $row['nri'];
            }
          }else {
            $nri = 0;
          }
          $totalAmount = ($count-$nri)*90000 + ($nri)*27000;
        ?>
        <!-- page content -->
        <div class="right_col" role="main">
          <!-- top tiles -->
          <div class="row tile_count">
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> Total Users</span>
              <div class="count"><?php echo $count; ?></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> Total Males</span>
              <div class="count"><?php echo $male; ?></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> Total Females</span>
              <div class="count"><?php echo $female; ?></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> Total Amount</span>
              <div class="count"><?php echo $totalAmount; ?></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-square"></i> No of Blocks</span>
              <div class="count"> 10</div>
              </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> Waiting Seats</span>
              <div class="count"> 20</div>
            </div>
          </div>
          <!-- /top tiles -->

          <!-- Number of capacity per block -->
          
      
          <!-- /Number of capacity per block -->
          <div class="row" style="width : 300,height:300">
            <div id="chartdiv" onload="resize();"></div>
          </div>
          <br />

          <div class="row">


            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><i class="fa fa-bars"></i> Notices and Messages <small>Send mass notice to all and message to particular MIS</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                                         <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <div class="col-xs-3">
                      <!-- required for floating -->
                      <!-- Nav tabs -->
                      <ul class="nav nav-tabs tabs-left">
                        <li class="active"><a href="#home" data-toggle="tab">All Notices</a>
                        </li>
                        <li><a href="#profile" data-toggle="tab">Update Notice</a>
                        </li>
                        <li><a href="#messages" data-toggle="tab">Delete Notice</a>
                        </li>
                        <li><a href="#settings" data-toggle="tab">Quick MSG</a>
                        </li>
                      </ul>
                    </div>

                    <div class="col-xs-9">
                      <!-- Tab panes -->
                      <div class="tab-content">
                        <div class="tab-pane active" id="home">
                          <!-- <p class="lead">All Notices</p>
                            <div class="panel panel-primary" id="result_panel">
                              <div class="panel-body">
                                  <ul class="list-group">
                                    <?php 
                                      $sql = "SELECT content FROM notices";
                                      $result = $conn->query($sql);
                                      if ($result->num_rows > 0) {
                                        // output data of each row
                                        $num = 0;
                                        $color = array("'list-group-item list-group-item-success'", "'list-group-item list-group-item-danger'"); 
                                        while($row = $result->fetch_assoc()) {
                                      ?>
                                      <li class=<?php echo $color[$num]; ?> class ="list-group-item"><strong><?php echo $row['content'];?></strong></li>
                                    <?php
                                        if($num == 0){
                                          $num = 1;
                                          }
                                          else{
                                            $num = 0;
                                          }
                                        }
                                        }else {

                                          $count = 0;
                                      }
                                    ?>
                                  </ul>
                              </div>
                         
                          </div> -->

                      <div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">
                      <?php 
                          $sql = "SELECT content FROM notices";
                          $result = $conn->query($sql);
                          if ($result->num_rows > 0) {
                            // output data of each row
                            $num = 0;
                            
                            while($row = $result->fetch_assoc()) {
                              $id = "'heading".$num."'";
                              $href = "'#collapse'".$num."'";
                              $cntrl = "'collapse'".$num."'";
                      ?>
                      <div class="panel">
                        <a class="panel-heading collapsed" role="tab" id=<?php echo $id; ?> data-toggle="collapse" data-parent="#accordion" href=<?php echo $href; ?> aria-expanded="false" aria-controls=<?php echo $cntrl; ?>>
                          <h4 class="panel-title"><?php echo $row['content']; ?></h4>
                        </a>
                        <div id=<?php echo $href; ?> class="panel-collapse collapse" role="tabpanel" aria-labelledby=<?php echo $id; ?>>
                          <div class="panel-body">
                            <p><strong>Collapsible Item 2 data</strong>
                            </p>
                          </div>
                        </div>
                      </div>
                      <?php
                            $num += 1;
                            }                        
                          }
                          else {
                            $count = 0;
                        }
                      ?>
                    </div>
                        </div>
                        <div class="tab-pane" id="profile">
                            <p class="lead">Update Message</p>
                            <div class="panel panel-primary" id="result_panel">
                              <div class="panel-body">
                                  <div class="container">
                                  <ul class="list-group">
                                    <?php 
                                      $sql = "SELECT content FROM notices";
                                      $result = $conn->query($sql);
                                      if ($result->num_rows > 0) {
                                        // output data of each row
                                        $num = 0;
                                        $color = array("'list-group-item list-group-item-success'", "'list-group-item list-group-item-info'"); 
                                        while($row = $result->fetch_assoc()) {
                                      ?>
                                        <div class="input-group">
                                        <li class=<?php echo $color[$num]; ?>><strong><?php echo $row['content'];?></strong></li>
                                        <span class="input-group-btn">
                                          <button class="btn btn-default" type="button" data-toggle="modal" data-target="#myModal">Update</button>
                                        </span>
                                      </div>
                                    <?php
                                        if($num == 0){
                                          $num = 1;
                                          }
                                          else{
                                            $num = 0;
                                          }
                                        }
                                        }else {

                                          $count = 0;
                                      }
                                    ?>
                                  </ul>
                                </div>
                              </div>
                         
                          </div>
                        </div>
                        <div class="tab-pane" id="messages">
                           <p class="lead">Delete Message</p>
                            <div class="panel panel-primary" id="result_panel">
                              <div class="panel-body">
                                  <div class="container">
                                  <ul class="list-group">
                                    <?php 
                                      $sql = "SELECT content FROM notices";
                                      $result = $conn->query($sql);
                                      if ($result->num_rows > 0) {
                                        // output data of each row
                                        $num = 0;
                                        $color = array("'list-group-item list-group-item-success'", "'list-group-item list-group-item-info'"); 
                                        while($row = $result->fetch_assoc()) {
                                      ?>
                                        <div class="input-group">
                                        <li class=<?php echo $color[$num]; ?> ><strong><?php echo $row['content'];?></strong></li>
                                        <span class="input-group-btn">
                                          <button class="btn btn-default" type="button">Delete</button>
                                        </span>
                                      </div>
                                    <?php
                                        if($num == 0){
                                          $num = 1;
                                          }
                                          else{
                                            $num = 0;
                                          }
                                        }
                                        }else {

                                          $count = 0;
                                      }
                                    ?>
                                  </ul>
                                </div>
                              </div>
                         
                          </div>

                        </div>
                        <div class="tab-pane" id="settings">
                          <div class="form-group">
                            <label for="usr">MIS:</label>
                            <input type="text" class="form-control" id="usr">
                          </div>
                          <div class="form-group">
                            <label for="comment">Comment:</label>
                            <textarea class="form-control" rows="5" id="comment"></textarea>
                          </div>
                          <button type="button" class="btn btn-success">Send</button>
                        </div>
                      </div>
                    </div>
                    </div>
                    <div class="clearfix"></div>

                  </div>
                </div>
              </div>
            <!-- Modal -->
              <div class="modal fade" id="myModal" role="dialog">
                <div class="modal-dialog">
                
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                      <h4 class="modal-title" id="myModalLabel">Update Notice</h4>
                    </div>
                    <div class="modal-body">
                      <textarea id="textareaID" class="form-control"></textarea>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                      <button type="button" class="btn btn-primary" data-dismiss="modal">Save changes</button>
                    </div>
                  </div><!-- /.modal-content -->
              </div>
              </div>
            
            
                <!-- end of weather widget -->
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
           COEP HOSTEL ADMISSION PORTAL
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="../vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="../vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="../vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="../vendors/skycons/skycons.js"></script>
    <!-- jQuery Sparklines -->
    <script src="../vendors/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
    <!-- Flot -->
    <script src="../vendors/Flot/jquery.flot.js"></script>
    <script src="../vendors/Flot/jquery.flot.pie.js"></script>
    <script src="../vendors/Flot/jquery.flot.time.js"></script>
    <script src="../vendors/Flot/jquery.flot.stack.js"></script>
    <script src="../vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="../vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="../vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="../vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="../vendors/DateJS/build/date.js"></script>
    <!-- JQVMap -->
    <script src="../vendors/jqvmap/dist/jquery.vmap.js"></script>
    <script src="../vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="../vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="../vendors/moment/min/moment.min.js"></script>
    <script src="../vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>
<!-- 	 <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>  -->
    <script type="text/javascript">
      $('button').click(function(){
         $('input[type="text"]').val('');
         $('textareaID').val('');
      });
    </script>
  </body>
</html>
<?php $conn->close(); ?>