<?php
include('../../auth.php');
require_once('../../connection.php');
?>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
     <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
     <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css" rel="stylesheet">
     

        <title></title>
        <script type="text/javascript">
            $(document).ready(function() {
    $('#example').DataTable();
} );
        </script>
        <style>
            

        </style>
</head>
<body>
    <div class="container">
    <table id="example" class="table table-responsive table-striped table-bordered " style="width:100%">
        <thead>
            <tr>
                <th>MIS</th>
                <th>Name</th>
                <th>Branch</th>
                <th>Year</th>
                <th>CGPA</th>
                <th>Category</th>
                <th>Reject</th>
            </tr>
        </thead>
        <tbody>
            <?php 
                $mis = '111603012';
                $name = 'Default N';
                $branch = 'comp';
                $year = 3;
                $cgpa = 6.7;
                $category = 'open';
                $sql1 = "select distinct(mis) as mis from status where accept = 1";
                #echo $sql1; 
                $result1 = $conn->query($sql1);
                if ($result1->num_rows > 0) {
                    while($row1 = $result1->fetch_assoc()) {
                        $mis = $row1['mis'];
                        $sql2 = "select mis,fname,lname,branch,year,category,cgpa from student where mis ='$mis'";
                        #echo $sql2;
                        $result2 = $conn->query($sql2);
                        if ($result2->num_rows > 0) {
                            while($row2 = $result2->fetch_assoc()) {
                                $name =$row2['fname'].' '.$row2['lname'];
                                $branch = $row2['branch'];
                                $year = $row2['year'];
                                $cgpa = $row2['cgpa'];
                                $category = $row2['category']; 
                            }
                        }
                        else{
                            $mis = '111603012';
                            $name = 'Default N';
                            $branch = 'comp';
                            $year = 3;
                            $cgpa = 6.7;
                            $category = 'open';
                        }    
            ?>
            <tr>
                <td><?php echo $mis; ?></td>
                <td><?php echo $name; ?></td>
                <td><?php echo $branch; ?></td>
                <td><?php echo $year; ?></td>
                <td><?php echo $cgpa; ?></td> 
                <td><?php echo $category; ?></td>
                <td><button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModal">Accept</button></td>
            </tr>
            <?php
                    }
                }
            ?>
        </tbody>
        <tfoot>
            <tr>
                <th>MIS</th>
                <th>Name</th>
                <th>Branch</th>
                <th>Year</th>
                <th>CGPA</th>
                <th>Category</th>
            </tr>
        </tfoot>
    </table>

<!-- Modal -->
        <div class="modal fade" id="myModal" role="dialog">
          <div class="modal-dialog modal-md">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Any Comments</h4>
              </div>
              <div class="modal-body">
                <textarea class="form-control" rows="5" id="comment" placeholder="Enter the reason for accepting student"></textarea>
              </div>
              <div class="modal-footer">
                <a href="deleteacceptedstudent.php"><button type="button" class="btn btn-success" data-dismiss="modal">Accept</button></a>
              </div>
            </div>
          </div>
        </div>
    </div><script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
 </body>   
</html>
<?php
$conn->close();
?>