<?php
    require('../../connection.php');
    // $boys = "CREATE VIEW boys as SELECT mis,caste,cgpa,year,branch,sbc,pwd,nri where gender=1 ORDER BY(cgpa) DESC";
    // $conn->query($boys);
    // $girls = "CREATE VIEW girls as SELECT mis,caste,cgpa,year,branch,sbc,pwd,nri where gender=0 ORDER BY(cgpa) DESC";
    // $conn->query($girls);

    // $tyBoys = "CREATE VIEW tyBoys as SELECT * from boys where year = 3 ORDER BY(cgpa) DESC";
    // $syBoys = "CREATE VIEW syBoys as SELECT * from boys where year = 3";
    // $tyBoys = "CREATE VIEW tyBoys as SELECT * from boys where year = 3";
    // $btBoys = "CREATE VIEW btBoys as SELECT * from boys where year = 3";

    // $tyGirls = "CREATE VIEW tyGirls as SELECT * from girls where year = 3";
    // $syGirls = "CREATE VIEW syGirls as SELECT * from girls where year = 3";
    // $tyGirls = "CREATE VIEW tyGirls as SELECT * from girls where year = 3";
    // $btGirls = "CREATE VIEW btGirls as SELECT * from girls where year = 3";
    
    $tyBoysMech = "SELECT  mis,category,cgpa,year,branch,sbc,pwd from student where year = 3 and gender=1 and branch = 'mech' and nri = 0 ORDER BY(cgpa) DESC";
    $result = $conn->query($tyBoysMech);
    if($result->num_rows > 0) {
        $count_open = 0;
        $sc = 6;
        $st = 3;
        $vj_ntb = 3;
        $ntc_ntd = 3;
        $obc = 9;
        while($row = $result->fetch_assoc()) {
            $mis = $row['mis'];
            $category = $row['category'];
            if($count_open < 24) {
                $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`,`branch`) VALUES ('$mis',1,'$category','mech')";
            }
            else{
                if($category == "vj/ntb" && $vj_ntb != 0){
                     $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`,`branch`) VALUES ('$mis',1,'$category','mech')";
                    $vj_ntb -= 1;
                }
                elseif($category == "ntc/ntd" && $ntc_ntd != 0){
                     $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`,`branch`) VALUES ('$mis',1,'$category','mech')";
                    $ntc_ntd -= 1;
                }
                elseif($category == "sc" && $sc != 0){
                     $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`,`branch`) VALUES ('$mis',1,'$category','mech')";
                    $sc -= 1;
                }
                elseif($category == "st" && $st != 0){
                     $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`,`branch`) VALUES ('$mis',1,'$category','mech')";
                    $st -= 1;
                }
                elseif($category == "obc" && $obc != 0){
                     $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`,`branch`) VALUES ('$mis',1,'$category','mech')";
                    $obc -= 1;
                }
                else{
                     $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`,`branch`) VALUES ('$mis',0,'$category','mech')";
                }
            }
            if($conn->query($insertIntoStatus)){

            }
            else{
                echo 'Error';
            }
            $count_open += 1;
        }
    }
    // echo $count_open . "mech boys done";
    $tyBoysCivil = "SELECT  mis,category,cgpa,year,branch,sbc,pwd from student where year = 3 and gender=1 and branch = 'civil' and nri = 0 ORDER BY(cgpa) DESC";
    $result = $conn->query($tyBoysCivil);
    if($result->num_rows > 0) {
        $count_open = 0;
        $sc = 3;
        $st = 2;
        $vj_ntb = 1;
        $ntc_ntd = 1;
        $obc = 5;
        while($row = $result->fetch_assoc()) {
            $mis = $row['mis'];
            $category = $row['category'];
            if($count_open < 12) {
                 $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`,`branch`) VALUES ('$mis',1,'$category','civil')";
            }
            else{
                if($category == "vj/ntb" && $vj_ntb != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`,`branch`) VALUES ('$mis',1,'$category','civil')";
                    $vj_ntb -= 1;
                }
                elseif($category == "ntc/ntd" && $ntc_ntd != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`,`branch`) VALUES ('$mis',1,'$category','civil')";
                    $ntc_ntd -= 1;
                }
                elseif($category == "sc" && $sc != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`,`branch`) VALUES ('$mis',1,'$category','civil')";
                    $sc -= 1;
                }
                elseif($category == "st" && $st != 0){
                   $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`,`branch`) VALUES ('$mis',1,'$category','civil')";
                    $st -= 1;
                }
                elseif($category == "obc" && $obc != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`,`branch`) VALUES ('$mis',1,'$category','civil')";
                    $obc -= 1;
                }
                else{
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`,`branch`) VALUES ('$mis',0,'$category','civil')";
                }
            }
            if($conn->query($insertIntoStatus)){

            }
            else{
                echo 'Error';
            }
            $count_open += 1;
        }
    }
    // echo $count_open . "civil boys done";
    $tyBoysComp = "SELECT  mis,category,cgpa,year,branch,sbc,pwd from student where year = 3 and gender=1 and branch = 'comp' and nri = 0 ORDER BY(cgpa) DESC";
    $result = $conn->query($tyBoysComp);
    if($result->num_rows > 0) {
        $count_open = 0;
        $sc = 3;
        $st = 2;
        $vj_ntb = 1;
        $ntc_ntd = 1;
        $obc = 5;
        while($row = $result->fetch_assoc()) {
            $mis = $row['mis'];
            $category = $row['category'];
            if($count_open < 12) {
                $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`,`branch`) VALUES ('$mis',1,'$category','comp')";
            }
            else{
                if($category == "vj/ntb" && $vj_ntb != 0){
                     $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`,`branch`) VALUES ('$mis',1,'$category','comp')";
                    $vj_ntb -= 1;
                }
                elseif($category == "ntc/ntd" && $ntc_ntd != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`,`branch`) VALUES ('$mis',1,'$category','comp')";
                    $ntc_ntd -= 1;
                }
                elseif($category == "sc" && $sc != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`,`branch`) VALUES ('$mis',1,'$category','comp')";
                    $sc -= 1;
                }
                elseif($category == "st" && $st != 0){
                     $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`,`branch`) VALUES ('$mis',1,'$category','comp')";
                    $st -= 1;
                }
                elseif($category == "obc" && $obc != 0){
                     $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`,`branch`) VALUES ('$mis',1,'$category','comp')";
                    $obc -= 1;
                }
                else{
                     $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`,`branch`) VALUES ('$mis',0,'$category','comp')";
                }
            }
            if($conn->query($insertIntoStatus)){

            }
            else{
                echo 'Error';
            }
            $count_open += 1;
        }
    }
    // echo $count_open . "civil boys done";
    $tyBoysElect = "SELECT  mis,category,cgpa,year,branch,sbc,pwd from student where year = 3 and gender=1 and branch = 'electrical' and nri = 0 ORDER BY(cgpa) DESC";
    $result = $conn->query($tyBoysElect);
    if($result->num_rows > 0) {
        $count_open = 0;
        $sc = 3;
        $st = 2;
        $vj_ntb = 1;
        $ntc_ntd = 1;
        $obc = 5;
        while($row = $result->fetch_assoc()) {
            $mis = $row['mis'];
            $category = $row['category'];
            if($count_open < 12) {
                 $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`,`branch`) VALUES ('$mis',1,'$category','electrical')";
            }
            else{
                if($category == "vj/ntb" && $vj_ntb != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`,`branch`) VALUES ('$mis',1,'$category','electrical')";
                    $vj_ntb -= 1;
                }
                elseif($category == "ntc/ntd" && $ntc_ntd != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`,`branch`) VALUES ('$mis',1,'$category','electrical')";
                    $ntc_ntd -= 1;
                }
                elseif($category == "sc" && $sc != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`,`branch`) VALUES ('$mis',1,'$category','electrical')";
                    $sc -= 1;
                }
                elseif($category == "st" && $st != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`,`branch`) VALUES ('$mis',1,'$category','electrical')";
                    $st -= 1;
                }
                elseif($category == "obc" && $obc != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`,`branch`) VALUES ('$mis',1,'$category','electrical')";
                    $obc -= 1;
                }
                else{
                   $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`,`branch`) VALUES ('$mis',0,'$category','electrical')";
                }
            }
            if($conn->query($insertIntoStatus)){

            }
            else{
                echo 'Error';
            }
            $count_open += 1;
        }
    }
    // echo $count_open . "civil boys done";
    $tyBoysEntc = "SELECT  mis,category,cgpa,year,branch,sbc,pwd from student where year = 3 and gender=1 and branch = 'entc' and nri = 0 ORDER BY(cgpa) DESC";
    $result = $conn->query($tyBoysEntc);
    if($result->num_rows > 0) {
        $count_open = 0;
        $sc = 3;
        $st = 2;
        $vj_ntb = 1;
        $ntc_ntd = 1;
        $obc = 5;
        while($row = $result->fetch_assoc()) {
            $mis = $row['mis'];
            $category = $row['category'];
            if($count_open < 12) {
                $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`,`branch`) VALUES ('$mis',1,'$category','entc')";
            }
            else{
                if($category == "vj/ntb" && $vj_ntb != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`,`branch`) VALUES ('$mis',1,'$category','entc')";
                    $vj_ntb -= 1;
                }
                elseif($category == "ntc/ntd" && $ntc_ntd != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`,`branch`) VALUES ('$mis',1,'$category','entc')";
                    $ntc_ntd -= 1;
                }
                elseif($category == "sc" && $sc != 0){
                   $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`,`branch`) VALUES ('$mis',1,'$category','entc')";
                    $sc -= 1;
                }
                elseif($category == "st" && $st != 0){
                 $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`,`branch`) VALUES ('$mis',1,'$category','entc')";
                    $st -= 1;
                }
                elseif($category == "obc" && $obc != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`,`branch`) VALUES ('$mis',1,'$category','entc')";
                    $obc -= 1;
                }
                else{
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`,`branch`) VALUES ('$mis',0,'$category','entc')";
                }
            }
            if($conn->query($insertIntoStatus)){

            }
            else{
                echo 'Error';
            }
            $count_open += 1;
        }
    }
    // echo $count_open . "civil boys done";
    $tyBoysIt = "SELECT  mis,category,cgpa,year,branch,sbc,pwd from student where year = 3 and gender=1 and branch = 'it' and nri = 0 ORDER BY(cgpa) DESC";
    $result = $conn->query($tyBoysIt);
    if($result->num_rows > 0) {
        $count_open = 0;
        $sc = 3;
        $st = 2;
        $vj_ntb = 1;
        $ntc_ntd = 1;
        $obc = 5;
        while($row = $result->fetch_assoc()) {
            $mis = $row['mis'];
            $category = $row['category'];
            if($count_open < 12) {
                $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`,`branch`) VALUES ('$mis',1,'$category','it')";
            }
            else{
                if($category == "vj/ntb" && $vj_ntb != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`,`branch`) VALUES ('$mis',1,'$category','it')";
                    $vj_ntb -= 1;
                }
                elseif($category == "ntc/ntd" && $ntc_ntd != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`,`branch`) VALUES ('$mis',1,'$category','it')";
                    $ntc_ntd -= 1;
                }
                elseif($category == "sc" && $sc != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`,`branch`) VALUES ('$mis',1,'$category','it')";
                    $sc -= 1;
                }
                elseif($category == "st" && $st != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`,`branch`) VALUES ('$mis',1,'$category','it')";
                    $st -= 1;
                }
                elseif($category == "obc" && $obc != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`,`branch`) VALUES ('$mis',1,'$category','it')";
                    $obc -= 1;
                }
                else{
                   $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`,`branch`) VALUES ('$mis',0,'$category','it')";
                }
            }
            if($conn->query($insertIntoStatus)){

            }
            else{
                echo 'Error';
            }
            $count_open += 1;
        }
    }
    // // echo $count_open . "civil boys done";
    $tyBoysMeta = "SELECT  mis,category,cgpa,year,branch,sbc,pwd from student where year = 3 and gender=1 and branch = 'meta' and nri = 0 ORDER BY(cgpa) DESC";
    $result = $conn->query($tyBoysMeta);
    if($result->num_rows > 0) {
        $count_open = 0;
        $sc = 3;
        $st = 2;
        $vj_ntb = 1;
        $ntc_ntd = 1;
        $obc = 5;
        while($row = $result->fetch_assoc()) {
            $mis = $row['mis'];
            $category = $row['category'];
            if($count_open < 12) {
                $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`,`branch`) VALUES ('$mis',1,'$category','meta')";
            }
            else{
                if($category == "vj/ntb" && $vj_ntb != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`,`branch`) VALUES ('$mis',1,'$category','meta')";
                    $vj_ntb -= 1;
                }
                elseif($category == "ntc/ntd" && $ntc_ntd != 0){
                   $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`,`branch`) VALUES ('$mis',1,'$category','meta')";
                    $ntc_ntd -= 1;
                }
                elseif($category == "sc" && $sc != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`,`branch`) VALUES ('$mis',1,'$category','meta')";
                    $sc -= 1;
                }
                elseif($category == "st" && $st != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`,`branch`) VALUES ('$mis',1,'$category','meta')";
                    $st -= 1;
                }
                elseif($category == "obc" && $obc != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`,`branch`) VALUES ('$mis',1,'$category','meta')";
                    $obc -= 1;
                }
                else{
                   $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`,`branch`) VALUES ('$mis',0,'$category','meta')";
                }
            }
            if($conn->query($insertIntoStatus)){

            }
            else{
                echo 'Error';
            }
            $count_open += 1;
        }
    }
    // // echo $count_open . "civil boys done";
    $tyBoysProd = "SELECT  mis,category,cgpa,year,branch,sbc,pwd from student where year = 3 and gender=1 and branch = 'prod' and nri = 0 ORDER BY(cgpa) DESC";
    $result = $conn->query($tyBoysProd);
    if($result->num_rows > 0) {
        $count_open = 0;
        $sc = 3;
        $st = 2;
        $vj_ntb = 1;
        $ntc_ntd = 1;
        $obc = 5;
        while($row = $result->fetch_assoc()) {
            $mis = $row['mis'];
            $category = $row['category'];
            if($count_open < 12) {
             $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`,`branch`) VALUES ('$mis',1,'$category','prod')";
            }
            else{
                if($category == "vj/ntb" && $vj_ntb != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`,`branch`) VALUES ('$mis',1,'$category','prod')";
                    $vj_ntb -= 1;
                }
                elseif($category == "ntc/ntd" && $ntc_ntd != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`,`branch`) VALUES ('$mis',1,'$category','prod')";
                    $ntc_ntd -= 1;
                }
                elseif($category == "sc" && $sc != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`,`branch`) VALUES ('$mis',1,'$category','prod')";
                    $sc -= 1;
                }
                elseif($category == "st" && $st != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`,`branch`) VALUES ('$mis',1,'$category','prod')";
                    $st -= 1;
                }
                elseif($category == "obc" && $obc != 0){
                   $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`,`branch`) VALUES ('$mis',1,'$category','prod')";
                    $obc -= 1;
                }
                else{
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`,`branch`) VALUES ('$mis',0,'$category','prod')";
                }
            }
            if($conn->query($insertIntoStatus)){

            }
            else{
                echo 'Error';
            }
            $count_open += 1;
        }
    }
    // // echo $count_open . "civil boys done";
    $tyBoysPlanning = "SELECT  mis,category,cgpa,year,branch,sbc,pwd from student where year = 3 and gender=1 and branch = 'planning' and nri = 0 ORDER BY(cgpa) DESC";
    $result = $conn->query($tyBoysPlanning);
    if($result->num_rows > 0) {
        $count_open = 0;
        $sc = 3;
        $st = 2;
        $vj_ntb = 1;
        $ntc_ntd = 1;
        $obc = 5;
        while($row = $result->fetch_assoc()) {
            $mis = $row['mis'];
            $category = $row['category'];
            if($count_open < 12) {
                $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`,`branch`) VALUES ('$mis',1,'$category','planning')";
            }
            else{
                if($category == "vj/ntb" && $vj_ntb != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`,`branch`) VALUES ('$mis',1,'$category','planning')";
                    $vj_ntb -= 1;
                }
                elseif($category == "ntc/ntd" && $ntc_ntd != 0){
                   $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`,`branch`) VALUES ('$mis',1,'$category','planning')";
                    $ntc_ntd -= 1;
                }
                elseif($category == "sc" && $sc != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`,`branch`) VALUES ('$mis',1,'$category','planning')";
                    $sc -= 1;
                }
                elseif($category == "st" && $st != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`,`branch`) VALUES ('$mis',1,'$category','planning')";
                    $st -= 1;
                }
                elseif($category == "obc" && $obc != 0){
                   $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`,`branch`) VALUES ('$mis',1,'$category','planning')";
                    $obc -= 1;
                }
                else{
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`,`branch`) VALUES ('$mis',0,'$category','planning')";
                }
            }
            if($conn->query($insertIntoStatus)){

            }
            else{
                echo 'Error';
            }
            $count_open += 1;
        }
    }
    // echo $count_open . "civil boys done";
    $tyBoysInstru = "SELECT  mis,category,cgpa,year,branch,sbc,pwd from student where year = 3 and gender=1 and branch = 'instru' and nri = 0 ORDER BY(cgpa) DESC";
    $result = $conn->query($tyBoysInstru);
    if($result->num_rows > 0) {
        $count_open = 0;
        $sc = 1;
        $st = 1;
        $vj_ntb = 1;
        $ntc_ntd = 1;
        $obc = 2;
        while($row = $result->fetch_assoc()) {
            $mis = $row['mis'];
            $category = $row['category'];
            if($count_open < 5) {
                $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`,`branch`) VALUES ('$mis',1,'$category','instru')";
            }
            else{
                if($category == "vj/ntb" && $vj_ntb != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`,`branch`) VALUES ('$mis',1,'$category','instru')";
                    $vj_ntb -= 1;
                }
                elseif($category == "ntc/ntd" && $ntc_ntd != 0){
                   $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`,`branch`) VALUES ('$mis',1,'$category','instru')";
                    $ntc_ntd -= 1;
                }
                elseif($category == "sc" && $sc != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`,`branch`) VALUES ('$mis',1,'$category','instru')";
                    $sc -= 1;
                }
                elseif($category == "st" && $st != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`,`branch`) VALUES ('$mis',1,'$category','instru')";
                    $st -= 1;
                }
                elseif($category == "obc" && $obc != 0){
                    $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`,`branch`) VALUES ('$mis',1,'$category','instru')";
                    $obc -= 1;
                }
                else{
                   $insertIntoStatus = "INSERT INTO `status`(`mis`, `accept`, `category`,`branch`) VALUES ('$mis',0,'$category','instru')";
                }
            }
            if($conn->query($insertIntoStatus)){

            }
            else{
                echo 'Error';
            }
            $count_open += 1;
        }
    }
    // echo $count_open . "civil boys done";
    echo "success..!";
?>
